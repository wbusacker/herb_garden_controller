import autoDisplay
import pygame
import sys
import time
import serial
import struct
import datetime
import math

class Telemetry_item:

    DATA_TYPE_SIZE_INDEX          = 0
    DATA_TYPE_DECODE_INDEX        = 1
    DATA_TYPE_STRING_FORMAT_INDEX = 2

    DATA_TYPE_INFO = {
        "uint8_t"  : [1, "B", "{:3d}"],
        "uint16_t" : [2, "H", "{:5d}"],
        "uint32_t" : [4, "I", "{:10d}"],
        "uint64_t" : [8, "Q", "{:20d}"],
        "int8_t"   : [1, "b", "{: 3d}"],
        "int16_t"  : [2, "h", "{: 5d}"],
        "int32_t"  : [4, "i", "{: 10d}"],
        "int64_t"  : [8, "q", "{: 20d}"],
        "float"    : [4, "f", "{:10.8e}"],
        "double"   : [8, "d", "{:19.17e}"]
    }

    def __init__(self, display_name, data_type, buffer_location, screen):

        self.data_type       = data_type
        self.buffer_location = int(buffer_location)
        self.screen          = screen
        self.screen_item_id  = screen.addDataSet(display_name)
        self.num_bytes       = Telemetry_item.DATA_TYPE_INFO[self.data_type][Telemetry_item.DATA_TYPE_SIZE_INDEX]
        self.decoder         = Telemetry_item.DATA_TYPE_INFO[self.data_type][Telemetry_item.DATA_TYPE_DECODE_INDEX]
        self.latest_data     = None
        self.string_format   = Telemetry_item.DATA_TYPE_INFO[self.data_type][Telemetry_item.DATA_TYPE_STRING_FORMAT_INDEX]

    def update(self, raw_buffer):

        data_bytes       = raw_buffer[self.buffer_location: self.buffer_location + self.num_bytes]
        self.latest_data = struct.unpack(self.decoder, data_bytes)[0]

        self.screen.update_point(self.screen_item_id, self.latest_data)
    
    def get_latest_data_string(self):
        return self.string_format.format(self.latest_data)

class Packet_processor:
    def __init__(self, packet_def_csv):

        self.screen              = autoDisplay.Pane("HGC Auto Display")
        self.tlm_items           = []

        tlm_file_name = "tlm_{:s}.csv".format(datetime.datetime.fromtimestamp(time.time()).strftime("%m-%d-%Y-%H-%M-%S"))
        self.tlm_file = open(tlm_file_name, "w")

        tlm_log_header = ""

        num_bytes_expected = 0
        with open(packet_def_csv, "r") as fh:
            for line in fh:
                data = line.split(",")
                self.tlm_items.append(Telemetry_item(data[0], data[1], data[2], self.screen))
                tlm_log_header += "{:s},".format(data[0])

                num_bytes_expected += Telemetry_item.DATA_TYPE_INFO[data[1]][Telemetry_item.DATA_TYPE_SIZE_INDEX]

        # The incoming packet takes two characters to make up one byte
        self.num_bytes_expected = int(math.ceil(num_bytes_expected / 7) * 8)

        self.tlm_file.write(tlm_log_header[:-1] + "\n")

        self.rx_packets_index = self.screen.addDataSet("Packet Count")
        self.rx_packets = 0

    def __del__(self):
        self.tlm_file.close()

    def update(self, tlm_data):

        valid_packet = False

        # Need to ignore the newline character representing end of data
        if((len(tlm_data) - 1) == self.num_bytes_expected):

            # Reconstruct the full buffer 8 bytes at a time
            byte_data = bytearray()

            for tlm_index in range(0,self.num_bytes_expected, 8):

                bit_overflow_data = tlm_data[tlm_index + 7]

                for i in range(0,7):
                    original_bit_7      = (bit_overflow_data << (i + 1)) & 0x80
                    original_lower_bits = tlm_data[tlm_index + i] & 0x7F

                    byte_data.append(original_lower_bits | original_bit_7)

            valid_packet = True

            log_line = ""

            for item in self.tlm_items:
                item.update(byte_data)

                log_line += "{:s},".format(item.get_latest_data_string())

            self.rx_packets += 1
            self.screen.update_point(self.rx_packets_index, self.rx_packets)

            self.tlm_file.write(log_line[:-1] + "\n")

        self.screen.display()
        return valid_packet


portID = 0
validPort = True
while(validPort):
    if portID > 20:
        print("No available COM devices available (Port must be under 20)")
        sys.exit(-1)
    try:
        p = "com" + str(portID)
        # print("Trying port %s" % p)
        ser = serial.Serial(port=p,baudrate=500000)
        ser.close()
        validPort = False
    except:
        portID += 1

if(len(sys.argv)== 2):
    portID = sys.argv[-1]

comPort = "com" + str(portID)

print("Opening port %s" % comPort)

ser = serial.Serial(port=comPort,baudrate=500000, timeout=0.1)

packet_handler = Packet_processor("tlm_map.csv")

while(True):
    
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_BACKSLASH:
                pygame.display.iconify()

    # Read in the line and figure out if we're reconfiguring or not
    raw_data = ser.readline()
    try:
        line = raw_data.decode("utf-8")
        if(len(line) > 1):
            print("Message: {:s}".format(line), end="")
    except:
        pass

    valid_packet = packet_handler.update(raw_data)
