/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  software
Filename: discrete_io_example.c
Purpose:  CSC test module
*/

#include <discrete_io_functions.h>
#include <test_engine.h>
