/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  Herb Garden Controller
Filename: spi_example.c
Purpose:  CSC test module
*/

#include <spi_functions.h>
#include <test_engine.h>
