/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  Herb Garden Controller
Filename: temp_probe_example.c
Purpose:  CSC test module
*/

#include <temp_probe_functions.h>
#include <test_engine.h>
