/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  software
Filename: mcp23s17_example.c
Purpose:  CSC test module
*/

#include <mcp23s17_functions.h>
#include <test_engine.h>
