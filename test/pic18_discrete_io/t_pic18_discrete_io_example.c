/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  software
Filename: pic18_io_example.c
Purpose:  CSC test module
*/

#include <pic18_io_functions.h>
#include <test_engine.h>
