/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  Herb Garden Controller
Filename: io.h
Purpose:  External CSC Header
*/

#ifndef IO_H
#define IO_H

/* CSC Includes */
#include <io_allocation.h>
#include <io_const.h>
#include <io_types.h>

/* Library Includes */
#include <stdint.h>

/* Project Includes */
#include <common.h>

enum Common_status_code init_io(void);
enum Common_status_code teardown_io(void);

void          io_set(enum IO_pin pin, enum IO_state state);
enum IO_state io_read(enum IO_pin pin);
void          io_set_pin_function(enum IO_pin pin, enum IO_pin_function function);

void io_set_local(enum IO_pin pin, enum IO_state state);

#endif
