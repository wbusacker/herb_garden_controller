/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  Herb Garden Controller
Filename: io_const.h
Purpose:  Constants used between all IO related modules and directly included by them
*/

#ifndef IO_COMMON_CONST_H
#define IO_COMMON_CONST_H

/* CSC Includes */

/* Library Includes */

/* Project Includes */

enum IO_pin_direction { IO_OUT, IO_IN };

enum IO_logic_state { IO_LOGIC_LOW, IO_LOGIC_HIGH };

#endif
