/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  Herb Garden Controller
Filename: io_template.c
Purpose:  CSC data declaration
*/

/* CSC Includes */
#include <io_functions.h>

/* Library Includes */

/* Project Includes */
