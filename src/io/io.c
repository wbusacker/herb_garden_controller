/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  Herb Garden Controller
Filename: io.c
Purpose:  CSC data and initialization definitions
*/

/* CSC Includes */
#include <io.h>
#include <io_data.h>
#include <io_functions.h>

/* Library Includes */

/* Project Includes */
#include <platform.h>

enum Common_status_code init_io(void) {
    enum Common_status_code return_code = COMMON_STATUS_OK;

    for (uint16_t i = 0; i < IO_PIN_COUNT; i++) {
        io_pin_config[i].active_mode = IO_ACTIVE_HIGH;
        io_pin_config[i].direction   = IO_IN;
        io_pin_config[i].function    = IO_PIN_FUNCTION_DISABLED;
    }

    return return_code;
}

enum Common_status_code teardown_io(void) {
    enum Common_status_code return_code = COMMON_STATUS_OK;
    return return_code;
}
