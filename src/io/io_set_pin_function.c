/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  Herb Garden Controller
Filename: io_template.c
Purpose:  CSC data declaration
*/

/* CSC Includes */
#include <io_functions.h>

/* Library Includes */

/* Project Includes */
#include <error_check.h>
#include <log.h>

void io_set_pin_function(enum IO_pin pin, enum IO_pin_function function) {

    ERROR_CHECK(pin >= IO_PIN_COUNT, "Unknown pin number %d\n", pin);

    io_pin_config[pin].function = function;

    switch (function) {
        case IO_PIN_FUNCTION_DISCRETE_OUT_ACTIVE_HIGH:
            io_pin_config[pin].active_mode = IO_ACTIVE_HIGH;
            io_pin_config[pin].direction   = IO_OUT;
            break;
        case IO_PIN_FUNCTION_DISCRETE_IN_ACTIVE_HIGH:
            io_pin_config[pin].active_mode = IO_ACTIVE_HIGH;
            io_pin_config[pin].direction   = IO_IN;
            break;
        case IO_PIN_FUNCTION_DISCRETE_OUT_ACTIVE_LOW:
            io_pin_config[pin].active_mode = IO_ACTIVE_LOW;
            io_pin_config[pin].direction   = IO_OUT;
            break;
        case IO_PIN_FUNCTION_DISCRETE_IN_ACTIVE_LOW:
            io_pin_config[pin].active_mode = IO_ACTIVE_LOW;
            io_pin_config[pin].direction   = IO_IN;
            break;
        default:
            break;
    }

    switch (io_pin_definitions[pin].type) {
        case IO_DEVICE_TYPE_PIC18:
            pic18_io_set_function(io_pin_definitions[pin].device_definition.pic18.pin_id,
                                  io_pin_function_definitions[function].pic18);
            break;
        case IO_DEVICE_TYPE_MCP23S17:
            mcp23s17_pin_set_function(io_pin_definitions[pin].device_definition.mcp23s17.device_id,
                                      io_pin_definitions[pin].device_definition.mcp23s17.pin_id,
                                      io_pin_function_definitions[function].mcp23s17);
            break;
        default:
            break;
    }

    LOG_DEBUG("Set Pin %d to function %d\n", pin, function);
}