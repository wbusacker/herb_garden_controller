/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  Herb Garden Controller
Filename: io_data.h
Purpose:  CSC data declaration
*/

#ifndef IO_DATA_H
#define IO_DATA_H

/* CSC Includes */
#include <io_allocation.h>
#include <io_const.h>
#include <io_types.h>

/* Library Includes */

/* Project Includes */
#include <common.h>

extern const struct IO_pin_definition          io_pin_definitions[IO_PIN_COUNT];
extern const struct IO_pin_function_definition io_pin_function_definitions[IO_PIN_FUNCTION_COUNT];

extern struct IO_pin_config io_pin_config[IO_PIN_COUNT];

#endif
