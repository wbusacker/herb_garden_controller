/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  Herb Garden Controller
Filename: io_functions.h
Purpose:  CSC local function declarations
*/

#ifndef IO_FUNCTIONS_H
#define IO_FUNCTIONS_H

/* CSC Includes */
#include <io.h>
#include <io_allocation.h>
#include <io_data.h>

/* Library Includes */

/* Project Includes */

void io_reset_pin_settings(uint8_t io_num);

#endif
