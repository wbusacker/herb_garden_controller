/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  Herb Garden Controller
Filename: io_types.h
Purpose:  CSC data types
*/

#ifndef IO_TYPES_H
#define IO_TYPES_H

/* CSC Includes */
#include <io_const.h>

/* Library Includes */
#include <stdbool.h>
#include <stdint.h>

/* Project Includes */
#include <mcp23s17.h>
#include <pic18_io.h>

struct IO_mcp23s17_interface {
    enum MCP23S17_device device_id;
    enum MCP23S17_pin    pin_id;
};

struct IO_pic18_interface {
    enum PIC18_io_pin pin_id;
};

union IO_device_interface_config {
    struct IO_mcp23s17_interface mcp23s17;
    struct IO_pic18_interface    pic18;
};

struct IO_pin_definition {
    enum IO_device_type              type;
    union IO_device_interface_config device_definition;
};

struct IO_pin_config {
    enum IO_pin_direction direction;
    enum IO_active        active_mode;
    enum IO_pin_function  function;
};

struct IO_pin_function_definition {
    enum MCP23S17_pin_function mcp23s17;
    enum PIC18_io_pin_function pic18;
};

#endif
