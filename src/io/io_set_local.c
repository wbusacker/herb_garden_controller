/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  Herb Garden Controller
Filename: io_template.c
Purpose:  CSC data declaration
*/

/* CSC Includes */
#include <io_functions.h>

/* Library Includes */

/* Project Includes */
#include <error_check.h>

void io_set_local(enum IO_pin pin, enum IO_state state) {

    ERROR_CHECK(pin >= IO_PIN_COUNT, "Unknown pin number %d\n", pin);
    ERROR_CHECK(io_pin_config[pin].direction == IO_IN, "Attempted to drive input pin %d\n", pin);
    ERROR_CHECK(io_pin_definitions[pin].type != IO_DEVICE_TYPE_PIC18, "Set local only allows PIC18 pins\n");

    enum IO_logic_state logic_state = IO_LOGIC_LOW;

    if (io_pin_config[pin].active_mode == IO_ACTIVE_LOW) {
        logic_state = state == IO_SET ? IO_LOGIC_LOW : IO_LOGIC_HIGH;
    } else {
        logic_state = state == IO_SET ? IO_LOGIC_HIGH : IO_LOGIC_LOW;
    }

    pic18_io_set(io_pin_definitions[pin].device_definition.pic18.pin_id, logic_state);
}