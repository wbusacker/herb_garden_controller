/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  Herb Garden Controller
Filename: io_set.c
Purpose:  CSC data declaration
*/

/* CSC Includes */
#include <io_functions.h>

/* Library Includes */

/* Project Includes */
#include <error_check.h>
#include <log.h>
#include <platform.h>

enum IO_state io_read(enum IO_pin pin) {

    ERROR_CHECK(pin >= IO_PIN_COUNT, "Unknown pin number %d\n", pin);

    enum IO_state       state       = IO_CLEAR;
    enum IO_logic_state logic_state = IO_LOGIC_LOW;

    switch (io_pin_definitions[pin].type) {
        case IO_DEVICE_TYPE_PIC18:
            logic_state = pic18_io_read(io_pin_definitions[pin].device_definition.pic18.pin_id);
            break;
        case IO_DEVICE_TYPE_MCP23S17:
            logic_state = mcp23s17_pin_read(io_pin_definitions[pin].device_definition.mcp23s17.device_id,
                                            io_pin_definitions[pin].device_definition.mcp23s17.pin_id);
            break;
        default:
            break;
    }

    if (io_pin_config[pin].active_mode == IO_ACTIVE_LOW) {
        state = logic_state == IO_LOGIC_LOW ? IO_SET : IO_CLEAR;
    } else {
        state = logic_state == IO_LOGIC_LOW ? IO_CLEAR : IO_SET;
    }

    return state;
}