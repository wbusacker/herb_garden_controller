/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  Herb Garden Controller
Filename: spi.h
Purpose:  External CSC Header
*/

#ifndef SPI_H
#define SPI_H

/* CSC Includes */
#include <spi_const.h>
#include <spi_types.h>

/* Library Includes */

/* Project Includes */
#include <common.h>
#include <io.h>

enum Common_status_code init_spi(void);
enum Common_status_code teardown_spi(void);

void spi_transaction(enum SPI_device id, uint8_t* tx_buffer, uint8_t* rx_buffer, uint8_t num_bytes);

#endif
