/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  Herb Garden Controller
Filename: spi.c
Purpose:  CSC data and initialization definitions
*/

/* CSC Includes */
#include <spi.h>
#include <spi_functions.h>

/* Library Includes */

/* Project Includes */
#include <log.h>

enum Common_status_code init_spi(void) {
    enum Common_status_code return_code = COMMON_STATUS_OK;

    io_set_pin_function(IO_PIN_SPI_MOSI, IO_PIN_FUNCTION_SPI_1_MOSI);
    io_set_pin_function(IO_PIN_SPI_MISO, IO_PIN_FUNCTION_SPI_1_MISO);
    io_set_pin_function(IO_PIN_SPI_CLOCK, IO_PIN_FUNCTION_SPI_1_CLOCK_IN);
    io_set_pin_function(IO_PIN_SPI_CLOCK, IO_PIN_FUNCTION_SPI_1_CLOCK_OUT);

    for (uint8_t i = 0; i < SPI_DEVICE_COUNT; i++) {

        enum IO_pin_function pin_function = IO_PIN_FUNCTION_DISCRETE_OUT_ACTIVE_HIGH;

        if (spi_device_type_defintions[spi_device_definitions[i].device_type].chip_select_active_mode ==
            IO_ACTIVE_LOW) {
            pin_function = IO_PIN_FUNCTION_DISCRETE_OUT_ACTIVE_LOW;
        }

        io_set_pin_function(spi_device_definitions[i].chip_select_io_number, pin_function);
        io_set_local(spi_device_definitions[i].chip_select_io_number, IO_CLEAR);
    }

    for (uint8_t i = 0; i < SPI_DEVICE_TYPE_DEFINITION_COUNT; i++) {

        double desired_speed_hz_d = spi_device_type_defintions[i].clock_hz;
        double desired_baud_d     = (COMMON_FOSC / (4.0 * desired_speed_hz_d)) - 1;

        if (desired_baud_d > 255) {
            desired_baud_d = 255;
            LOG_ALWAYS("SPI Device Type %d clamped to max baud\n", i);
        }

        uint8_t ssp_add_value = (uint8_t)desired_baud_d;
        if (ssp_add_value < 3) {
            ssp_add_value = 3;
        }

        spi_device_baud_rates[i] = ssp_add_value;

        LOG_DEBUG("SPI Device Type %d calculated baud %d\n", i, spi_device_baud_rates[i]);
    }

    /* Reference MSSP Control Register definitions for value meanings */

    SSP1STATbits.SMP = 0;
    SSP1STATbits.CKE = 0;

    SSP1CON1bits.WCOL  = 0;
    SSP1CON1bits.SSPOV = 0;
    SSP1CON1bits.SSPEN = 1;
    SSP1CON1bits.CKP   = 0;
    SSP1CON1bits.SSPM  = 0b1010;

    SSP1CON3bits.BOEN = 0;

    return return_code;
}

enum Common_status_code teardown_spi(void) {
    enum Common_status_code return_code = COMMON_STATUS_OK;

    return return_code;
}
