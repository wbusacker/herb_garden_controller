/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  Herb Garden Controller
Filename: spi_data.h
Purpose:  CSC data declaration
*/

#ifndef SPI_DATA_H
#define SPI_DATA_H

/* CSC Includes */
#include <spi_const.h>
#include <spi_types.h>

/* Library Includes */

/* Project Includes */

extern const struct SPI_device_type_definition spi_device_type_defintions[SPI_DEVICE_TYPE_DEFINITION_COUNT];
extern const struct SPI_device_definition      spi_device_definitions[SPI_DEVICE_COUNT];

/* Keep the calculated baud rates seperate so the compiler will put the definitions in program memory and not ram */
extern uint8_t spi_device_baud_rates[SPI_DEVICE_TYPE_DEFINITION_COUNT];

#endif
