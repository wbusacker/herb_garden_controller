/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  Herb Garden Controller
Filename: spi_types.h
Purpose:  CSC data types
*/

#ifndef SPI_TYPES_H
#define SPI_TYPES_H

/* CSC Includes */
#include <spi_const.h>
/* Library Includes */
#include <stdint.h>

/* Project Includes */
#include <io.h>

struct SPI_device_type_definition {
    uint32_t       clock_hz;
    enum SPI_mode  mode;
    enum IO_active chip_select_active_mode;
};

struct SPI_device_definition {
    enum IO_pin          chip_select_io_number;
    enum SPI_device_type device_type;
};

#endif
