/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  Herb Garden Controller
Filename: spi_functions.h
Purpose:  CSC local function declarations
*/

#ifndef SPI_FUNCTIONS_H
#define SPI_FUNCTIONS_H

/* CSC Includes */
#include <spi.h>
#include <spi_data.h>

/* Library Includes */

/* Project Includes */

uint8_t spi_send_8(uint8_t tx_byte);

#endif
