/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  Herb Garden Controller
Filename: spi_template.c
Purpose:  Example Function File
*/

/* CSC Includes */
#include <spi_functions.h>

/* Library Includes */

/* Project Includes */

uint8_t spi_send_8(uint8_t tx_byte) {

    SSP1BUF = tx_byte;

    /* Wait for the byte to come in */
    while (SSP1STATbits.BF == 0)
        ;

    return SSP1BUF;
}