/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  Herb Garden Controller
Filename: spi_data.c
Purpose:  CSC data definition
*/

/* CSC Includes */
#include <spi_data.h>

/* Library Includes */

/* Project Includes */

// clang-format off

const struct SPI_device_type_definition spi_device_type_defintions[SPI_DEVICE_TYPE_DEFINITION_COUNT] = {
    [SPI_DEVICE_TYPE_SN74HC164] = {.mode = SPI_MODE_0, .clock_hz = 250000, .chip_select_active_mode = IO_ACTIVE_HIGH},
    [SPI_DEVICE_TYPE_MCP23S17]  = {.mode = SPI_MODE_0, .clock_hz = 125000, .chip_select_active_mode = IO_ACTIVE_LOW},
    [SPI_DEVICE_TYPE_MCP3304]   = {.mode = SPI_MODE_0, .clock_hz = 125000, .chip_select_active_mode = IO_ACTIVE_LOW},
};

const struct SPI_device_definition spi_device_definitions[SPI_DEVICE_COUNT] = {
    [SPI_DEVICE_IO_EXPANDER_0_0] = {.device_type = SPI_DEVICE_TYPE_MCP23S17, .chip_select_io_number = IO_PIN_CARD_0_IO_EXPANDER_0_CS},
    [SPI_DEVICE_IO_EXPANDER_0_1] = {.device_type = SPI_DEVICE_TYPE_MCP23S17, .chip_select_io_number = IO_PIN_CARD_0_IO_EXPANDER_1_CS},
    [SPI_DEVICE_IO_EXPANDER_0_2] = {.device_type = SPI_DEVICE_TYPE_MCP23S17, .chip_select_io_number = IO_PIN_CARD_0_IO_EXPANDER_2_CS},
    [SPI_DEVICE_IO_EXPANDER_0_3] = {.device_type = SPI_DEVICE_TYPE_MCP23S17, .chip_select_io_number = IO_PIN_CARD_0_IO_EXPANDER_3_CS},
    [SPI_DEVICE_IO_EXPANDER_0_4] = {.device_type = SPI_DEVICE_TYPE_MCP23S17, .chip_select_io_number = IO_PIN_CARD_0_IO_EXPANDER_4_CS},
    [SPI_DEVICE_IO_EXPANDER_1_0] = {.device_type = SPI_DEVICE_TYPE_MCP23S17, .chip_select_io_number = IO_PIN_CARD_1_IO_EXPANDER_0_CS},
    [SPI_DEVICE_IO_EXPANDER_1_1] = {.device_type = SPI_DEVICE_TYPE_MCP23S17, .chip_select_io_number = IO_PIN_CARD_1_IO_EXPANDER_1_CS},
    [SPI_DEVICE_IO_EXPANDER_1_2] = {.device_type = SPI_DEVICE_TYPE_MCP23S17, .chip_select_io_number = IO_PIN_CARD_1_IO_EXPANDER_2_CS},
    [SPI_DEVICE_IO_EXPANDER_1_3] = {.device_type = SPI_DEVICE_TYPE_MCP23S17, .chip_select_io_number = IO_PIN_CARD_1_IO_EXPANDER_3_CS},
    [SPI_DEVICE_IO_EXPANDER_1_4] = {.device_type = SPI_DEVICE_TYPE_MCP23S17, .chip_select_io_number = IO_PIN_CARD_1_IO_EXPANDER_4_CS},
    [SPI_DEVICE_IO_EXPANDER_2_0] = {.device_type = SPI_DEVICE_TYPE_MCP23S17, .chip_select_io_number = IO_PIN_CARD_2_IO_EXPANDER_0_CS},
    [SPI_DEVICE_IO_EXPANDER_2_1] = {.device_type = SPI_DEVICE_TYPE_MCP23S17, .chip_select_io_number = IO_PIN_CARD_2_IO_EXPANDER_1_CS},
    [SPI_DEVICE_IO_EXPANDER_2_2] = {.device_type = SPI_DEVICE_TYPE_MCP23S17, .chip_select_io_number = IO_PIN_CARD_2_IO_EXPANDER_2_CS},
    [SPI_DEVICE_IO_EXPANDER_2_3] = {.device_type = SPI_DEVICE_TYPE_MCP23S17, .chip_select_io_number = IO_PIN_CARD_2_IO_EXPANDER_3_CS},
    [SPI_DEVICE_IO_EXPANDER_2_4] = {.device_type = SPI_DEVICE_TYPE_MCP23S17, .chip_select_io_number = IO_PIN_CARD_2_IO_EXPANDER_4_CS},
    [SPI_DEVICE_MCP3304_0]       = {.device_type = SPI_DEVICE_TYPE_MCP3304,  .chip_select_io_number = IO_PIN_CARD_0_ADC_0},
    [SPI_DEVICE_MCP3304_1]       = {.device_type = SPI_DEVICE_TYPE_MCP3304,  .chip_select_io_number = IO_PIN_CARD_0_ADC_1},
    [SPI_DEVICE_MCP3304_2]       = {.device_type = SPI_DEVICE_TYPE_MCP3304,  .chip_select_io_number = IO_PIN_CARD_0_ADC_2},
    [SPI_DEVICE_MCP3304_3]       = {.device_type = SPI_DEVICE_TYPE_MCP3304,  .chip_select_io_number = IO_PIN_CARD_1_ADC_0},
    [SPI_DEVICE_MCP3304_4]       = {.device_type = SPI_DEVICE_TYPE_MCP3304,  .chip_select_io_number = IO_PIN_CARD_1_ADC_1},
    [SPI_DEVICE_MCP3304_5]       = {.device_type = SPI_DEVICE_TYPE_MCP3304,  .chip_select_io_number = IO_PIN_CARD_1_ADC_2},
    [SPI_DEVICE_MCP3304_6]       = {.device_type = SPI_DEVICE_TYPE_MCP3304,  .chip_select_io_number = IO_PIN_CARD_2_ADC_0},
    [SPI_DEVICE_MCP3304_7]       = {.device_type = SPI_DEVICE_TYPE_MCP3304,  .chip_select_io_number = IO_PIN_CARD_2_ADC_1},
    [SPI_DEVICE_MCP3304_8]       = {.device_type = SPI_DEVICE_TYPE_MCP3304,  .chip_select_io_number = IO_PIN_CARD_2_ADC_2},
};

// clang-format on

uint8_t spi_device_baud_rates[SPI_DEVICE_TYPE_DEFINITION_COUNT];