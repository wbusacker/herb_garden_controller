/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  Herb Garden Controller
Filename: spi_template.c
Purpose:  Example Function File
*/

/* CSC Includes */
#include <spi_functions.h>

/* Library Includes */
#include <stddef.h>

/* Project Includes */
#include <error_check.h>
#include <log.h>

void spi_transaction(enum SPI_device id, uint8_t* tx_buffer, uint8_t* rx_buffer, uint8_t num_bytes) {

    ERROR_CHECK(id >= SPI_DEVICE_COUNT, "Unknown device %d\n", id);

    static enum SPI_device_type last_configed_device = SPI_DEVICE_TYPE_DEFINITION_COUNT;

    if (last_configed_device != spi_device_definitions[id].device_type) {

        /* Disable the SPI device while we're reconfiguring */
        SSP1CON1bits.SSPEN = 0;

        SSP1ADD = spi_device_baud_rates[spi_device_definitions[id].device_type];

        switch (spi_device_type_defintions[spi_device_definitions[id].device_type].mode) {
            case SPI_MODE_0:
                SSP1STATbits.CKE = 1;
                SSP1STATbits.SMP = 0;
                SSP1CON1bits.CKP = 0;
                break;
            case SPI_MODE_1:
                SSP1STATbits.CKE = 1;
                SSP1STATbits.SMP = 1;
                SSP1CON1bits.CKP = 0;

                break;
            case SPI_MODE_2:
                SSP1STATbits.CKE = 0;
                SSP1STATbits.SMP = 0;
                SSP1CON1bits.CKP = 1;
                break;
            case SPI_MODE_3:
                SSP1STATbits.CKE = 1;
                SSP1STATbits.SMP = 1;
                SSP1CON1bits.CKP = 1;
                break;
        }

        SSP1CON1bits.SSPEN = 1;

        /* If we switched mode, transmit a dummy byte out to no one so get
            all the devices used to the new settings
        */
        spi_send_8(0);

        last_configed_device = spi_device_definitions[id].device_type;
    }

    io_set_local(spi_device_definitions[id].chip_select_io_number, IO_SET);

    for (uint8_t i = 0; i < num_bytes; i++) {

        uint8_t data_to_send = 0;
        if (tx_buffer != NULL) {
            data_to_send = tx_buffer[i];
        }

        uint8_t rx_data = spi_send_8(data_to_send);

        if (rx_buffer != NULL) {
            rx_buffer[i] = rx_data;
        }
    }

    io_set_local(spi_device_definitions[id].chip_select_io_number, IO_CLEAR);
}