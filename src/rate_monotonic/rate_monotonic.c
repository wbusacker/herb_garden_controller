/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  Herb Garden Controller
Filename: rate_monotonic.c
Purpose:  CSC data and initialization definitions
*/

/* CSC Includes */
#include <rate_monotonic.h>
#include <rate_monotonic_functions.h>

/* Library Includes */

/* Project Includes */

enum Common_status_code init_rate_monotonic(void) {
    enum Common_status_code return_code = COMMON_STATUS_OK;

    return return_code;
}

enum Common_status_code teardown_rate_monotonic(void) {
    enum Common_status_code return_code = COMMON_STATUS_OK;

    return return_code;
}
