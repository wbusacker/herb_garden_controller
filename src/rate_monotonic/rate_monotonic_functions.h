/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  Herb Garden Controller
Filename: rate_monotonic_functions.h
Purpose:  CSC local function declarations
*/

#ifndef RATE_MONOTONIC_FUNCTIONS_H
#define RATE_MONOTONIC_FUNCTIONS_H

/* CSC Includes */
#include <rate_monotonic.h>
#include <rate_monotonic_data.h>

/* Library Includes */

/* Project Includes */

#endif
