/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  Herb Garden Controller
Filename: rate_monotonic_data.c
Purpose:  CSC data definition
*/

/* CSC Includes */
#include <rate_monotonic_data.h>

/* Library Includes */

/* Project Includes */
