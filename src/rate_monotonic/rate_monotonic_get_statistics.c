/*
 Copyright (c) 2024 Will Busacker
 See project license for more details

 Project:  Herb Garden Controller
 Filename: rate_monotonic_template.c
 Purpose:  Example Function File
 */

/* CSC Includes */
#include <rate_monotonic_functions.h>

/* Library Includes */

/* Project Includes */

void rate_monotonic_get_statistics(struct Rate_monotonic_t* rm,
                                   int64_t*                 average_time_ns,
                                   int64_t*                 shortest_time_ns,
                                   int64_t*                 longest_time_ns,
                                   int64_t*                 missed) {

    *shortest_time_ns = rm->shortest_cycle_ns;
    *longest_time_ns  = rm->longest_cycle_ns;
    *average_time_ns  = rm->total_awake_ns / rm->total_periods;
    *missed           = rm->missed_periods;

    return;
}