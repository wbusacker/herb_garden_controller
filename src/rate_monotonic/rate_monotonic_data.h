/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  Herb Garden Controller
Filename: rate_monotonic_data.h
Purpose:  CSC data declaration
*/

#ifndef RATE_MONOTONIC_DATA_H
#define RATE_MONOTONIC_DATA_H

/* CSC Includes */
#include <rate_monotonic_const.h>
#include <rate_monotonic_types.h>

/* Library Includes */

/* Project Includes */

#endif
