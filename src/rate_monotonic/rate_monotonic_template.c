/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  Herb Garden Controller
Filename: rate_monotonic_template.c
Purpose:  Example Function File
*/

/* CSC Includes */
#include <rate_monotonic_functions.h>

/* Library Includes */

/* Project Includes */
