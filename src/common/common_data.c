/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  tcars
Filename: common_data.c
Purpose:  CSC data definition
*/

/* CSC Includes */
#include <common_data.h>

/* Library Includes */

/* Project Includes */

const char* common_status_code_strings[COMMON_STATUS_MAX_CODES + 1] =
  {"OK", "INITIALIZATION_ERROR", "SHUTDOWN_REQUESTED", "CYCLE_ERROR", "BAD_MESSAGE_LEN", "UNKNOWN_CODE"};