/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  tcars
Filename: common.c
Purpose:  CSC data and initialization definitions
*/

/* CSC Includes */
#include <common.h>
#include <common_functions.h>

/* Library Includes */

/* Project Includes */

enum Common_status_code init_common(void) {
    return COMMON_STATUS_OK;
}

enum Common_status_code teardown_common(void) {
    return COMMON_STATUS_OK;
}
