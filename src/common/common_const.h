/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  tcars
Filename: common_const.h
Purpose:  CSC constants
*/

/* CSC Includes */

/* Library Includes */

/* Project Includes */

#ifndef COMMON_CONST_H
#define COMMON_CONST_H
#define COMMON_NAME_MAX_LEN (128)

#define COMMON_BITS_PER_BYTE   (8)
#define COMMON_BITS_PER_NIBBLE (4)

#define COMMON_NIBBLE_MASK (0xF)
#define COMMON_BYTE_MASK   (0xFF)

#define COMMON_CYCLE_RATE_HZ   (1000)
#define COMMON_CYCLE_PERIOD_NS (1E9 / COMMON_CYCLE_RATE_HZ)

#define COMMON_FOSC (16000000ll)
#define COMMON_FCY  (COMMON_FOSC / 4)

#define COMMON_RAIL_5V (5.166f)

/* Don't forget to update the string codes in _data.c */
enum Common_status_code {
    COMMON_STATUS_OK,
    COMMON_STATUS_INITIALIZATION_ERROR,
    COMMON_STATUS_SHUTDOWN_REQUESTED,
    COMMON_STATUS_CYCLE_ERROR,
    COMMON_STATUS_BAD_MESSAGE_LEN,
    COMMON_STATUS_GENERAL_FAILURE,
    COMMON_STATUS_MAX_CODES
};

enum Common_register_field_width {
    COMMON_REGISTER_TYPE_8_BIT,
    COMMON_REGISTER_TYPE_16_BIT,
    COMMON_REGISTER_TYPE_32_BIT,
    COMMON_REGISTER_TYPE_64_BIT,
};

#endif
