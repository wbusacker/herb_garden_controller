/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  tcars
Filename: common_types.h
Purpose:  CSC data types
*/

#ifndef COMMON_TYPES_H
#define COMMON_TYPES_H
#include <common_const.h>
#include <stdbool.h>
#include <stdint.h>

union Common_register_field_pointer {
    volatile uint8_t*  byte;
    volatile uint16_t* word;
    volatile uint32_t* dword;
    volatile uint64_t* qword;
};

struct Common_register_field {
    enum Common_register_field_width    register_width;
    union Common_register_field_pointer pointer;
    uint8_t                             bit_length;
    uint8_t                             bit_offset;
} __attribute__((__packed__));

#endif
