/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  tcars
Filename: common_template.c
Purpose:  Example Function File
*/

/* CSC Includes */
#include <common_functions.h>

/* Library Includes */

/* Project Includes */

const char* common_get_status_code_string(enum Common_status_code code) {

    enum Common_status_code lookup = code;
    if (code > COMMON_STATUS_MAX_CODES) {
        lookup = COMMON_STATUS_MAX_CODES;
    }

    return common_status_code_strings[lookup];
}