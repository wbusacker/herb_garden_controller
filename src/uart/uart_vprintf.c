/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  Herb Garden Controller
Filename: uart.c
Purpose:  CSC data and initialization definitions
*/

/* CSC Includes */
#include <uart_functions.h>

/* Library Includes */
#include <stdarg.h>
#include <stdio.h>

/* Project Includes */
#include <platform.h>

void uart_vprintf(const char* format, va_list arg_list) {

    if (uart_out_enabled) {

        static uint8_t uart_vprintf_buffer[UART_MAX_PRINTF_LEN];

        int num_bytes = vsnprintf((char*)uart_vprintf_buffer, UART_MAX_PRINTF_LEN - 1, format, arg_list);

        if (num_bytes == -1) {
            num_bytes = UART_MAX_PRINTF_LEN - 1;
        }
        uart_send_buffer(uart_vprintf_buffer, (uint16_t)num_bytes);
    }
}