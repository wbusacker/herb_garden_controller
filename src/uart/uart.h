/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  Herb Garden Controller
Filename: uart.h
Purpose:  External CSC Header
*/

#ifndef UART_H
#define UART_H

/* CSC Includes */
#include <uart_const.h>
#include <uart_types.h>

/* Library Includes */
#include <stdarg.h>
#include <stdint.h>

/* Project Includes */
#include <common.h>

enum Common_status_code init_uart(void);
enum Common_status_code teardown_uart(void);

uint16_t uart_send_buffer(const uint8_t* buffer, uint16_t len);
uint16_t uart_collect_buffer(uint8_t* buffer, uint16_t len);
void     uart_configure_baud(uint64_t desired_baud);

void uart_printf(char* format, ...);
void uart_vprintf(const char* format, va_list arg_list);

#endif
