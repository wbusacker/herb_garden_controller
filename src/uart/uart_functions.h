/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  Herb Garden Controller
Filename: uart_functions.h
Purpose:  CSC local function declarations
*/

#ifndef UART_FUNCTIONS_H
#define UART_FUNCTIONS_H

/* CSC Includes */
#include <uart.h>
#include <uart_data.h>

/* Library Includes */

/* Project Includes */

#endif
