/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  Herb Garden Controller
Filename: uart.c
Purpose:  CSC data and initialization definitions
*/

/* CSC Includes */
#include <uart.h>
#include <uart_data.h>

/* Library Includes */

/* Project Includes */
#include <io.h>
#include <platform.h>

enum Common_status_code init_uart(void) {
    enum Common_status_code return_code = COMMON_STATUS_OK;

    uart_incoming_write_head = 0;
    uart_incoming_read_head  = 0;

    io_set_pin_function(IO_PIN_UART_RX, IO_PIN_FUNCTION_UART_1_RX);
    io_set_pin_function(IO_PIN_UART_TX, IO_PIN_FUNCTION_UART_1_TX);

    /* Calculate Baud Generator rate */
    uart_configure_baud(UART_SYSTEM_BAUD_RATE);

    RC1STA   = 0b10000000;
    TX1STA   = 0b10100100;
    BAUD1CON = 0b00001000;
    // SP1BRGH  = 0;
    // SP1BRGL  = 32;

    uart_out_enabled = true;

    uart_printf("\n\n");
    uart_printf("House Control Unit\n");
    uart_printf("Serial Startup\n");

    return return_code;
}

enum Common_status_code teardown_uart(void) {
    enum Common_status_code return_code = COMMON_STATUS_OK;

    return return_code;
}