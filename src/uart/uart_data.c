/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  Herb Garden Controller
Filename: uart_data.h
Purpose:  CSC data declaration
*/

/* CSC Includes */
#include <uart_data.h>

/* Library Includes */

/* Project Includes */

volatile uint8_t  uart_incoming_data_buffer[UART_DATA_HOLDING_BUFFER_LEN];
volatile uint16_t uart_incoming_write_head;
volatile uint16_t uart_incoming_read_head;

bool uart_out_enabled = false;