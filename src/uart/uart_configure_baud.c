/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  Herb Garden Controller
Filename: uart.c
Purpose:  CSC data and initialization definitions
*/

/* CSC Includes */
#include <uart_functions.h>

/* Library Includes */

/* Project Includes */
#include <platform.h>

void uart_configure_baud(uint64_t desired_baud) {

    double fcy        = COMMON_FOSC;
    double fmd        = UART_BAUD_DIVIDER;
    double db         = desired_baud;
    double fast_brg_d = (fcy / (fmd * db)) - 1;

    uint16_t fast_brg_i = (uint16_t)fast_brg_d;

    SP1BRGH = (uint8_t)((fast_brg_i >> COMMON_BITS_PER_BYTE) & COMMON_BYTE_MASK);
    SP1BRGL = (uint8_t)(fast_brg_i & COMMON_BYTE_MASK);
}