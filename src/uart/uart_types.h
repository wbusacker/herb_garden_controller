/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  Herb Garden Controller
Filename: uart_types.h
Purpose:  CSC data types
*/

#ifndef UART_TYPES_H
#define UART_TYPES_H

/* CSC Includes */
#include <uart_const.h>

/* Library Includes */

/* Project Includes */

#endif
