/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  Herb Garden Controller
Filename: uart.c
Purpose:  CSC data and initialization definitions
*/

/* CSC Includes */

/* Library Includes */

/* Project Includes */
#include <platform.h>
#include <uart_functions.h>

uint16_t uart_send_buffer(const uint8_t* buffer, uint16_t len) {

    uint16_t sent_chars = 0;
    if (uart_out_enabled) {

        uint16_t num_chars = len;
        if (len == 0) {
            while (buffer[num_chars] != 0) {
                num_chars++;
            }
        }

        for (sent_chars = 0; sent_chars < num_chars; sent_chars++) {
            TX1REG = buffer[sent_chars];
            while ((TX1STA & (1 << 1)) == 0) {
                __asm__ volatile("nop");
            }
        }
    }

    return sent_chars;
}