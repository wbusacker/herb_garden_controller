/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  software
Filename: mcp3304.h
Purpose:  External CSC Header
*/

#ifndef MCP3304_H
#define MCP3304_H

/* CSC Includes */
#include <mcp3304_const.h>
#include <mcp3304_types.h>

/* Library Includes */

/* Project Includes */

#include <common.h>

enum Common_status_code init_mcp3304(void);
enum Common_status_code teardown_mcp3304(void);

enum Common_status_code cycle_mcp3304(uint64_t cycle_count, void* arg);

int16_t mcp3304_get_channel(enum MCP3304_channel channel);

#endif
