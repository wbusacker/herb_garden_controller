/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  software
Filename: mcp3304_const.h
Purpose:  CSC constants
*/

#ifndef MCP3304_CONST_H
#define MCP3304_CONST_H

/* CSC Includes */
#include <mcp3304_allocation.h>

/* Library Includes */

/* Project Includes */

#define MCP3304_MAX_CODE (4096)

enum MCP3304_channel_bits {
    MCP3304_CHANNEL_DIFF_0 = 0b000,
    MCP3304_CHANNEL_DIFF_1 = 0b010,
    MCP3304_CHANNEL_DIFF_2 = 0b100,
    MCP3304_CHANNEL_DIFF_3 = 0b110
};

#endif
