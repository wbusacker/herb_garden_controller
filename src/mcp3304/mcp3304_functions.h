/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  software
Filename: mcp3304_functions.h
Purpose:  CSC local function declarations
*/

#ifndef MCP3304_FUNCTIONS_H
#define MCP3304_FUNCTIONS_H

/* CSC Includes */
#include <mcp3304.h>
#include <mcp3304_data.h>

/* Library Includes */

/* Project Includes */

#endif
