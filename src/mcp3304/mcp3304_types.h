/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  software
Filename: mcp3304_types.h
Purpose:  CSC data types
*/

#ifndef MCP3304_TYPES_H
#define MCP3304_TYPES_H

/* CSC Includes */
#include <mcp3304_const.h>

/* Library Includes */

/* Project Includes */
#include <spi.h>
#include <telemetry.h>

struct MCP3304_channel_definition {
    enum Telemetry_measurand_ids tlm_id;
    enum SPI_device              spi_id;
    enum MCP3304_channel_bits    channel;
};

#endif
