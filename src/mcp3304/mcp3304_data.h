/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  software
Filename: mcp3304_data.h
Purpose:  CSC data declaration
*/

#ifndef MCP3304_DATA_H
#define MCP3304_DATA_H

/* CSC Includes */
#include <mcp3304_const.h>
#include <mcp3304_types.h>

/* Library Includes */

/* Project Includes */

// clang-format off
extern const struct MCP3304_channel_definition mcp3304_channel_definitions[MCP3304_CHANNEL_COUNT];

// clang-format on

#endif
