/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  software
Filename: mcp3304_const.h
Purpose:  CSC constants
*/

#ifndef MCP3304_ALLOCATION_H
#define MCP3304_ALLOCATION_H

/* CSC Includes */

/* Library Includes */

/* Project Includes */

enum MCP3304_channel {
    MCP3304_CHANNEL_0,
    MCP3304_CHANNEL_1,
    MCP3304_CHANNEL_2,
    MCP3304_CHANNEL_3,
    MCP3304_CHANNEL_4,
    MCP3304_CHANNEL_5,
    MCP3304_CHANNEL_6,
    MCP3304_CHANNEL_7,
    MCP3304_CHANNEL_8,
    MCP3304_CHANNEL_9,
    MCP3304_CHANNEL_10,
    MCP3304_CHANNEL_11,
    MCP3304_CHANNEL_12,
    MCP3304_CHANNEL_13,
    MCP3304_CHANNEL_14,
    MCP3304_CHANNEL_15,
    MCP3304_CHANNEL_16,
    MCP3304_CHANNEL_17,
    MCP3304_CHANNEL_18,
    MCP3304_CHANNEL_19,
    MCP3304_CHANNEL_20,
    MCP3304_CHANNEL_21,
    MCP3304_CHANNEL_22,
    MCP3304_CHANNEL_23,
    MCP3304_CHANNEL_24,
    MCP3304_CHANNEL_25,
    MCP3304_CHANNEL_26,
    MCP3304_CHANNEL_27,
    MCP3304_CHANNEL_28,
    MCP3304_CHANNEL_29,
    MCP3304_CHANNEL_30,
    MCP3304_CHANNEL_31,
    MCP3304_CHANNEL_32,
    MCP3304_CHANNEL_33,
    MCP3304_CHANNEL_34,
    MCP3304_CHANNEL_35,
    MCP3304_CHANNEL_COUNT
};

#endif
