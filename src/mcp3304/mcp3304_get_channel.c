/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  software
Filename: mcp3304_template.c
Purpose:  Example Function File
*/

/* CSC Includes */
#include <mcp3304_functions.h>

/* Library Includes */

/* Project Includes */
#include <error_check.h>
#include <message_mcp3304_autocode.h>
#include <spi.h>

int16_t mcp3304_get_channel(enum MCP3304_channel channel) {

    ERROR_CHECK(channel >= MCP3304_CHANNEL_COUNT, "Unknown channel %d\n", channel);

    uint8_t tx_buffer[MESSAGE_MCP3304_ADC_READ_BUFFER_LEN] = {0b00001000, 0b00000000, 0b00000000};

    uint8_t rx_buffer[MESSAGE_MCP3304_ADC_READ_BUFFER_LEN];

    tx_buffer[0] |= (mcp3304_channel_definitions[channel].channel >> 1) & 0x3;
    tx_buffer[1] |= (mcp3304_channel_definitions[channel].channel & 0b1) << 7;

    spi_transaction(mcp3304_channel_definitions[channel].spi_id,
                    tx_buffer,
                    rx_buffer,
                    MESSAGE_MCP3304_ADC_READ_BUFFER_LEN);

    int16_t data = ((int16_t)rx_buffer[1] & 0x1F) << 8;
    data |= rx_buffer[2];

    if ((data & 0x1000) != 0) {
        data |= 0xF000;
    }

    telemetry_buffer_measurand_int16_t(mcp3304_channel_definitions[channel].tlm_id, data);

    return data;
}