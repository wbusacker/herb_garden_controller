/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  software
Filename: mcp3304_cycle.c
Purpose:  CSC data and initialization definitions
*/

/* CSC Includes */
#include <mcp3304.h>
#include <mcp3304_functions.h>

/* Library Includes */

/* Project Includes */

enum Common_status_code cycle_mcp3304(uint64_t cycle_count, void* arg) {
    enum Common_status_code return_code = COMMON_STATUS_OK;

    return return_code;
}
