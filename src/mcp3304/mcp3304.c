/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  software
Filename: mcp3304.c
Purpose:  CSC data and initialization definitions
*/

/* CSC Includes */
#include <mcp3304.h>
#include <mcp3304_functions.h>

/* Library Includes */

/* Project Includes */

enum Common_status_code init_mcp3304(void) {
    enum Common_status_code return_code = COMMON_STATUS_OK;

    return return_code;
}

enum Common_status_code teardown_mcp3304(void) {
    enum Common_status_code return_code = COMMON_STATUS_OK;

    return return_code;
}
