/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  Herb Garden Controller
Filename: interrupts.c
Purpose:  External CSC Header
*/

/* CSC Includes */
#include <uptime.h>

/* Library Includes */
#include <xc.h>

/* Project Includes */

void __interrupt() interrupt_main_handler(void) {

    if (TMR0IF) {
        uptime_interrupt();
        TMR0IF = 0;
    }
}
