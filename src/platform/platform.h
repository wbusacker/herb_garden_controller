/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  Herb Garden Controller
Filename: platform.h
Purpose:  External CSC Header
*/

#ifndef PLATFORM_H
#define PLATFORM_H

/* CSC Includes */

/* Library Includes */
#include <xc.h>

/* Project Includes */

#define BASE_CLOCK_FREQUENCY_HZ         (16E6ll)
#define TIMER_BASE_DIVIDER              (2)
#define PERIPHERICAL_CLOCK_FREQUENCY_HZ (BASE_CLOCK_FREQUENCY_HZ / TIMER_BASE_DIVIDER)

void platform_init(void);

#endif
