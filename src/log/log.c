/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  p3x
Filename: log.c
Purpose:  CSC data and initialization definitions
*/

/* CSC Includes */
#include <log.h>
#include <log_functions.h>

/* Library Includes */
#include <string.h>

/* Project Includes */
#include <uptime.h>

enum Common_status_code init_log(void) {
    enum Common_status_code return_code = COMMON_STATUS_OK;

    static bool already_init = false;

    if (already_init == false) {
        already_init = true;

        log_system_level = LOG_LEVEL_DEBUG;

        log_time_start_ns = uptime_nanoseconds();

        LOG_INFO("Starting log\n");
    }

    return return_code;
}

enum Common_status_code teardown_log(void) {
    enum Common_status_code return_code = COMMON_STATUS_OK;
    return return_code;
}
