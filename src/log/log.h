/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  p3x
Filename: log.h
Purpose:  External CSC Header
*/

#ifndef LOG_H
#define LOG_H

/* CSC Includes */
#include <log_const.h>
#include <log_types.h>

/* Library Includes */
#include <stdint.h>

/* Project Includes */
#include <common.h>

enum Common_status_code init_log(void);
enum Common_status_code teardown_log(void);

void log_printf(enum LOG_LEVELS level, const char* file_name, int line_number, const char* format, ...);
void log_no_header_printf(const char* format, ...);

#ifdef ENABLE_DEBUG

    /* DEPRICATED: Use other Log functions */
    #define LOG(...) log_printf(LOG_LEVEL_ALWAYS, __FILE__, __LINE__, __VA_ARGS__)

    /* Send a message to program log always */
    #define LOG_ALWAYS(...) log_printf(LOG_LEVEL_ALWAYS, __FILE__, __LINE__, __VA_ARGS__)

    /* Send a critical message to program log. Use for fatal errors */
    #define LOG_CRITICAL(...) log_printf(LOG_LEVEL_CRITICAL, __FILE__, __LINE__, __VA_ARGS__)

    /* Send a general error message to program log. Use for recoverable errors */
    #define LOG_ERROR(...) log_printf(LOG_LEVEL_ERROR, __FILE__, __LINE__, __VA_ARGS__)

    /* Send a warning message to program log. */
    #define LOG_WARNING(...) log_printf(LOG_LEVEL_WARNING, __FILE__, __LINE__, __VA_ARGS__)

    /* Send a general message to program log. */
    #define LOG_INFO(...) log_printf(LOG_LEVEL_INFO, __FILE__, __LINE__, __VA_ARGS__)

    /* Good old PRINTF Debugging */
    #define LOG_DEBUG(...) log_printf(LOG_LEVEL_DEBUG, __FILE__, __LINE__, __VA_ARGS__)

#else

    #define LOG(...)
    #define LOG_ALWAYS(...) log_no_header_printf(__VA_ARGS__)
    #define LOG_CRITICAL(...)
    #define LOG_ERROR(...)
    #define LOG_WARNING(...)
    #define LOG_INFO(...)
    #define LOG_DEBUG(...)

#endif

void log_set_global_level(enum LOG_LEVELS level);

#endif
