/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  p3x
Filename: log_const.h
Purpose:  CSC constants
*/

#ifndef LOG_CONST_H
#define LOG_CONST_H

/* CSC Includes */

/* Library Includes */

/* Project Includes */
#include <platform.h>

#define LOG_LEVEL_STRING_LEN (5)

#define LOG_STANDARD_HEADER_FORMAT "%15lld | %s | %50s:%-4d | "

#define LOG_INVALID_PATTERN_ID LOG_MAXIMUM_PATTERNS

/*
    Levels of severity for the requested message.
    Rank in order such that when a log level is set,
    any level equal or lower value will be put to
    the log, all others will be dropped
*/

enum LOG_LEVELS {
    LOG_LEVEL_NEVER = 0,
    LOG_LEVEL_ALWAYS,
    LOG_LEVEL_CRITICAL,
    LOG_LEVEL_ERROR,
    LOG_LEVEL_WARNING,
    LOG_LEVEL_INFO,
    LOG_LEVEL_DEBUG,
    LOG_LEVEL_COUNT
};

#endif
