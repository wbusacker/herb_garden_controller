/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  p3x
Filename: log_template.c
Purpose:  Example Function File
*/

/* CSC Includes */
#include <log_functions.h>

/* Library Includes */
#include <stdarg.h>

/* Project Includes */
#include <uart.h>

void log_no_header_printf(const char* format, ...) {

    va_list arg_list;
    va_start(arg_list, format);
    uart_vprintf(format, arg_list);
    va_end(arg_list);
}
