/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  p3x
Filename: log_data.h
Purpose:  CSC data declaration
*/

#ifndef LOG_DATA_H
#define LOG_DATA_H

/* CSC Includes */
#include <log_const.h>
#include <log_types.h>

/* Library Includes */
#include <stdint.h>

/* Project Includes */

extern uint64_t log_time_start_ns;

extern char log_level_string_definitions[LOG_LEVEL_COUNT][LOG_LEVEL_STRING_LEN];

extern enum LOG_LEVELS log_system_level;

#endif
