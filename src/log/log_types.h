/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  p3x
Filename: log_types.h
Purpose:  CSC data types
*/

#ifndef LOG_TYPES_H
#define LOG_TYPES_H

/* CSC Includes */
#include <log_const.h>

/* Library Includes */
#include <stdbool.h>

/* Project Includes */

#endif
