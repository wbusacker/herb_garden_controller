/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  p3x
Filename: log_template.c
Purpose:  Example Function File
*/

/* CSC Includes */
#include <log_functions.h>

/* Library Includes */
#include <stdarg.h>
#include <stdio.h>
#include <string.h>

/* Project Includes */
#include <uart.h>
#include <uptime.h>

void log_printf(enum LOG_LEVELS level, const char* file_name, int line_number, const char* format, ...) {

    /* Figure out if we should surpress this or not */
    if (level <= log_system_level) {
        va_list arg_list;
        va_start(arg_list, format);

        uart_printf(LOG_STANDARD_HEADER_FORMAT,
                    uptime_nanoseconds() - log_time_start_ns,
                    log_level_string_definitions[level],
                    file_name,
                    line_number);

        uart_vprintf(format, arg_list);

        va_end(arg_list);
    }
}