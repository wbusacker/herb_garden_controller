/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  p3x
Filename: log_functions.h
Purpose:  CSC local function declarations
*/

#ifndef LOG_FUNCTIONS_H
#define LOG_FUNCTIONS_H

/* CSC Includes */
#include <log.h>
#include <log_data.h>

/* Library Includes */

/* Project Includes */

#endif
