/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  p3x
Filename: log_template.c
Purpose:  Example Function File
*/

/* CSC Includes */
#include <log_functions.h>

/* Library Includes */

/* Project Includes */

void log_set_global_level(enum LOG_LEVELS level) {
    log_system_level = level;
}