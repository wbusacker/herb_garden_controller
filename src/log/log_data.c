/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  p3x
Filename: log_data.c
Purpose:  CSC data definition
*/

/* CSC Includes */
#include <log_data.h>

/* Library Includes */
#include <stdlib.h>

/* Project Includes */

uint64_t log_time_start_ns = 0;

char log_level_string_definitions[LOG_LEVEL_COUNT][LOG_LEVEL_STRING_LEN] =
  {"NEV ", "ALWY", "CRIT", "ERR ", "WARN", "INFO", "DBG "};

enum LOG_LEVELS log_system_level;