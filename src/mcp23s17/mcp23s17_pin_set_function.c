/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  software
Filename: mcp23s17_template.c
Purpose:  Example Function File
*/

/* CSC Includes */
#include <mcp23s17_functions.h>

/* Library Includes */

/* Project Includes */
#include <error_check.h>
#include <log.h>

void mcp23s17_pin_set_function(enum MCP23S17_device       device,
                               enum MCP23S17_pin          pin,
                               enum MCP23S17_pin_function function) {

    switch (function) {
        case MCP23S17_PIN_FUNCTION_DISCRETE_OUT:
            mcp23s17_pin_set_direction(device, pin, IO_OUT);
            break;
        case MCP23S17_PIN_FUNCTION_DISCRETE_IN:
            mcp23s17_pin_set_direction(device, pin, IO_IN);
            break;
        default:
            ERROR("Unkown pin function %d\n", function);
            break;
    }
}
