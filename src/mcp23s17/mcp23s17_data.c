/*
 Copyright (c) 2024 Will Busacker
 See project license for more details

 Project:  software
 Filename: mcp23s17_data.c
 Purpose:  CSC data definition
 */

/* CSC Includes */
#include <mcp23s17_data.h>

/* Library Includes */

/* Project Includes */

// clang-format off
const struct MCP23S17_device_definition mcp23s17_device_definitions[MCP23S17_DEVICE_COUNT] = {
    [MCP23S17_DEVICE_GARDEN_BOARD_0_0] = {.spi_id = SPI_DEVICE_IO_EXPANDER_0_0},
    [MCP23S17_DEVICE_GARDEN_BOARD_0_1] = {.spi_id = SPI_DEVICE_IO_EXPANDER_0_1},
    [MCP23S17_DEVICE_GARDEN_BOARD_0_2] = {.spi_id = SPI_DEVICE_IO_EXPANDER_0_2},
    [MCP23S17_DEVICE_GARDEN_BOARD_0_3] = {.spi_id = SPI_DEVICE_IO_EXPANDER_0_3},
    [MCP23S17_DEVICE_GARDEN_BOARD_0_4] = {.spi_id = SPI_DEVICE_IO_EXPANDER_0_4},
    [MCP23S17_DEVICE_GARDEN_BOARD_1_0] = {.spi_id = SPI_DEVICE_IO_EXPANDER_1_0},
    [MCP23S17_DEVICE_GARDEN_BOARD_1_1] = {.spi_id = SPI_DEVICE_IO_EXPANDER_1_1},
    [MCP23S17_DEVICE_GARDEN_BOARD_1_2] = {.spi_id = SPI_DEVICE_IO_EXPANDER_1_2},
    [MCP23S17_DEVICE_GARDEN_BOARD_1_3] = {.spi_id = SPI_DEVICE_IO_EXPANDER_1_3},
    [MCP23S17_DEVICE_GARDEN_BOARD_1_4] = {.spi_id = SPI_DEVICE_IO_EXPANDER_1_4},
    [MCP23S17_DEVICE_GARDEN_BOARD_2_0] = {.spi_id = SPI_DEVICE_IO_EXPANDER_2_0},
    [MCP23S17_DEVICE_GARDEN_BOARD_2_1] = {.spi_id = SPI_DEVICE_IO_EXPANDER_2_1},
    [MCP23S17_DEVICE_GARDEN_BOARD_2_2] = {.spi_id = SPI_DEVICE_IO_EXPANDER_2_2},
    [MCP23S17_DEVICE_GARDEN_BOARD_2_3] = {.spi_id = SPI_DEVICE_IO_EXPANDER_2_3},
    [MCP23S17_DEVICE_GARDEN_BOARD_2_4] = {.spi_id = SPI_DEVICE_IO_EXPANDER_2_4},
};

const struct MCP23S17_pin_definition mcp23s17_pin_definitions[MCP23S17_PIN_COUNT] = {
    [MCP23S17_PIN_1]  = {.port = MCP23S17_PORT_B,    .discrete_bit = 0},
    [MCP23S17_PIN_2]  = {.port = MCP23S17_PORT_B,    .discrete_bit = 1},
    [MCP23S17_PIN_3]  = {.port = MCP23S17_PORT_B,    .discrete_bit = 2},
    [MCP23S17_PIN_4]  = {.port = MCP23S17_PORT_B,    .discrete_bit = 3},
    [MCP23S17_PIN_5]  = {.port = MCP23S17_PORT_B,    .discrete_bit = 4},
    [MCP23S17_PIN_6]  = {.port = MCP23S17_PORT_B,    .discrete_bit = 5},
    [MCP23S17_PIN_7]  = {.port = MCP23S17_PORT_B,    .discrete_bit = 6},
    [MCP23S17_PIN_8]  = {.port = MCP23S17_PORT_B,    .discrete_bit = 7},
    [MCP23S17_PIN_21] = {.port = MCP23S17_PORT_A,    .discrete_bit = 0},
    [MCP23S17_PIN_22] = {.port = MCP23S17_PORT_A,    .discrete_bit = 1},
    [MCP23S17_PIN_23] = {.port = MCP23S17_PORT_A,    .discrete_bit = 2},
    [MCP23S17_PIN_24] = {.port = MCP23S17_PORT_A,    .discrete_bit = 3},
    [MCP23S17_PIN_25] = {.port = MCP23S17_PORT_A,    .discrete_bit = 4},
    [MCP23S17_PIN_26] = {.port = MCP23S17_PORT_A,    .discrete_bit = 5},
    [MCP23S17_PIN_27] = {.port = MCP23S17_PORT_A,    .discrete_bit = 6},
    [MCP23S17_PIN_28] = {.port = MCP23S17_PORT_A,    .discrete_bit = 7}
};
// clang-format on