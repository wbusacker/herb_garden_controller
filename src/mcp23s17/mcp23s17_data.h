/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  software
Filename: mcp23s17_data.h
Purpose:  CSC data declaration
*/

#ifndef MCP23S17_DATA_H
#define MCP23S17_DATA_H

/* CSC Includes */
#include <mcp23s17_const.h>
#include <mcp23s17_types.h>

/* Library Includes */

/* Project Includes */

extern const struct MCP23S17_device_definition mcp23s17_device_definitions[MCP23S17_DEVICE_COUNT];
extern const struct MCP23S17_pin_definition    mcp23s17_pin_definitions[MCP23S17_PIN_COUNT];

#endif
