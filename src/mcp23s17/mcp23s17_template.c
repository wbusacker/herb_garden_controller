/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  software
Filename: mcp23s17_template.c
Purpose:  Example Function File
*/

/* CSC Includes */
#include <mcp23s17_functions.h>

/* Library Includes */

/* Project Includes */
