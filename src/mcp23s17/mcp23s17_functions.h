/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  software
Filename: mcp23s17_functions.h
Purpose:  CSC local function declarations
*/

#ifndef MCP23S17_FUNCTIONS_H
#define MCP23S17_FUNCTIONS_H

/* CSC Includes */
#include <mcp23s17.h>
#include <mcp23s17_data.h>

/* Library Includes */

/* Project Includes */

void    mcp23s17_device_write(enum MCP23S17_device device, enum MCP23S17_register_address address, uint8_t data);
uint8_t mcp23s17_device_read(enum MCP23S17_device device, enum MCP23S17_register_address address);
void    mcp23s17_pin_set_direction(enum MCP23S17_device device, enum MCP23S17_pin pin, enum IO_pin_direction direction);
#endif
