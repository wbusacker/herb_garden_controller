/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  software
Filename: mcp23s17_template.c
Purpose:  Example Function File
*/

/* CSC Includes */
#include <mcp23s17_functions.h>

/* Library Includes */

/* Project Includes */
#include <error_check.h>
#include <log.h>

enum IO_logic_state mcp23s17_pin_read(enum MCP23S17_device device, enum MCP23S17_pin pin) {

    ERROR_CHECK(device >= MCP23S17_DEVICE_COUNT, "Unknown device %d\n", device);
    ERROR_CHECK(pin >= MCP23S17_PIN_COUNT, "Unknown pin %d\n", pin);

    enum IO_logic_state state = IO_LOGIC_LOW;

    enum MCP23S17_register_address addr = MCP23S17_REGISTER_PORT;

    if (mcp23s17_pin_definitions[pin].port == MCP23S17_PORT_B) {
        addr += MCP23S17_PORT_BANK_OFFSET;
    }

    uint8_t data = mcp23s17_device_read(device, addr);
    uint8_t bit  = (data >> mcp23s17_pin_definitions[pin].discrete_bit) & 1;

    state = bit == 0 ? IO_LOGIC_LOW : IO_LOGIC_HIGH;

    return state;
}