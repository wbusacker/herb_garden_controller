/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  software
Filename: mcp23s17.h
Purpose:  External CSC Header
*/

#ifndef MCP23S17_H
#define MCP23S17_H

/* CSC Includes */
#include <mcp23s17_const.h>
#include <mcp23s17_types.h>

/* Library Includes */

/* Project Includes */
#include <common.h>
#include <io_common_const.h>

enum Common_status_code init_mcp23s17(void);
enum Common_status_code teardown_mcp23s17(void);

void                mcp23s17_pin_set(enum MCP23S17_device device, enum MCP23S17_pin pin, enum IO_logic_state state);
enum IO_logic_state mcp23s17_pin_read(enum MCP23S17_device device, enum MCP23S17_pin pin);
void mcp23s17_pin_set_function(enum MCP23S17_device device, enum MCP23S17_pin pin, enum MCP23S17_pin_function function);

#endif
