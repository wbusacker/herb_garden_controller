/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  software
Filename: mcp23s17_types.h
Purpose:  CSC data types
*/

#ifndef MCP23S17_TYPES_H
#define MCP23S17_TYPES_H

/* CSC Includes */
#include <mcp23s17_const.h>

/* Library Includes */

/* Project Includes */
#include <spi.h>

struct MCP23S17_device_definition {
    enum SPI_device spi_id;
};

struct MCP23S17_pin_definition {
    enum MCP23S17_port port;
    uint8_t            discrete_bit;
};

#endif
