/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  software
Filename: mcp23s17_const.h
Purpose:  CSC constants
*/

#ifndef MCP23S17_CONST_H
#define MCP23S17_CONST_H

/* CSC Includes */
#include <mcp23s17_allocation.h>

/* Library Includes */

/* Project Includes */

#define MCP23S17_PORT_BANK_OFFSET (0x10)

enum MCP23S17_register_address {
    MCP23S17_REGISTER_IO_DIRECTION             = 0x00,
    MCP23S17_REGISTER_IO_POLARITY              = 0x01,
    MCP23S17_REGISTER_INTERRUPT_ENABLE         = 0x02,
    MCP23S17_REGISTER_INTERRUPT_DEFAULT_VALUE  = 0x03,
    MCP23S17_REGISTER_INTERRUPT_CONTROL        = 0x04,
    MCP23S17_REGISTER_DEVICE_CONTROL           = 0x05,
    MCP23S17_REGISTER_PULL_UP_ENABLE           = 0x06,
    MCP23S17_REGISTER_INTERRUPT_FLAG           = 0x07,
    MCP23S17_REGISTER_INTERRUPT_CAPTURED_VALUE = 0x08,
    MCP23S17_REGISTER_PORT                     = 0x09,
    MCP23S17_REGISTER_LATCH                    = 0x0A,
    MCP23S17_REGISTER_DEFAULT_DEVICE_CONTROL   = 0x0B
};

enum MCP23S17_port { MCP23S17_PORT_A, MCP23S17_PORT_B, MCP23S17_PORT_NONE };

enum MCP23S17_message_mode { MCP23S17_MESSAGE_WRITE = 0, MCP23S17_MESSAGE_READ = 1 };

enum MCP23S17_pin_function {
    MCP23S17_PIN_FUNCTION_DISCRETE_OUT,
    MCP23S17_PIN_FUNCTION_DISCRETE_IN,
    MCP23S17_PIN_FUNCTION_COUNT
};

#endif
