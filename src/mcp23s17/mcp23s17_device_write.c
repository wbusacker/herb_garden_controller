/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  software
Filename: mcp23s17_template.c
Purpose:  Example Function File
*/

/* CSC Includes */
#include <mcp23s17_functions.h>

/* Library Includes */

/* Project Includes */
#include <log.h>
#include <message_mcp23s17_autocode.h>

void mcp23s17_device_write(enum MCP23S17_device device, enum MCP23S17_register_address address, uint8_t data) {

    uint8_t tx_message[MESSAGE_MCP23S17_REGISTER_ACCESS_BUFFER_LEN] = {0b01000000, address, data};

    spi_transaction(mcp23s17_device_definitions[device].spi_id,
                    tx_message,
                    NULL,
                    MESSAGE_MCP23S17_REGISTER_ACCESS_BUFFER_LEN);
}