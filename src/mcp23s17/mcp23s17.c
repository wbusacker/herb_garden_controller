/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  software
Filename: mcp23s17.c
Purpose:  CSC data and initialization definitions
*/

/* CSC Includes */
#include <mcp23s17.h>
#include <mcp23s17_functions.h>

/* Library Includes */

/* Project Includes */

enum Common_status_code init_mcp23s17(void) {
    enum Common_status_code return_code = COMMON_STATUS_OK;

    for (uint8_t i = 0; i < MCP23S17_DEVICE_COUNT; i++) {

        /* Change the addressing mode for the device */
        mcp23s17_device_write(i, MCP23S17_REGISTER_DEFAULT_DEVICE_CONTROL, 0b10110100);

        for (uint8_t j = 0; j < MCP23S17_PORT_NONE; j++) {

            uint8_t offset = j == 0 ? 0 : MCP23S17_PORT_BANK_OFFSET;

            mcp23s17_device_write(i, offset + MCP23S17_REGISTER_IO_DIRECTION, 0xFF);
            mcp23s17_device_write(i, offset + MCP23S17_REGISTER_IO_POLARITY, 0x00);
            mcp23s17_device_write(i, offset + MCP23S17_REGISTER_INTERRUPT_ENABLE, 0x00);
            mcp23s17_device_write(i, offset + MCP23S17_REGISTER_INTERRUPT_DEFAULT_VALUE, 0x00);
            mcp23s17_device_write(i, offset + MCP23S17_REGISTER_INTERRUPT_CAPTURED_VALUE, 0x00);
            mcp23s17_device_write(i, offset + MCP23S17_REGISTER_INTERRUPT_CONTROL, 0x00);
            mcp23s17_device_write(i, offset + MCP23S17_REGISTER_INTERRUPT_FLAG, 0x00);
            mcp23s17_device_write(i, offset + MCP23S17_REGISTER_PULL_UP_ENABLE, 0x00);
            mcp23s17_device_write(i, offset + MCP23S17_REGISTER_PORT, 0x00);
            mcp23s17_device_write(i, offset + MCP23S17_REGISTER_LATCH, 0x00);
        }
    }

    return return_code;
}

enum Common_status_code teardown_mcp23s17(void) {
    enum Common_status_code return_code = COMMON_STATUS_OK;

    return return_code;
}
