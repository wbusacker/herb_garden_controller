/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  software
Filename: mcp23s17_template.c
Purpose:  Example Function File
*/

/* CSC Includes */
#include <mcp23s17_functions.h>

/* Library Includes */

/* Project Includes */
#include <log.h>
#include <message_mcp23s17_autocode.h>

uint8_t mcp23s17_device_read(enum MCP23S17_device device, enum MCP23S17_register_address address) {

    uint8_t tx_message[MESSAGE_MCP23S17_REGISTER_ACCESS_BUFFER_LEN] = {0b01000001, address, 0};

    uint8_t rx_message[MESSAGE_MCP23S17_REGISTER_ACCESS_BUFFER_LEN];

    spi_transaction(mcp23s17_device_definitions[device].spi_id,
                    tx_message,
                    rx_message,
                    MESSAGE_MCP23S17_REGISTER_ACCESS_BUFFER_LEN);

    return rx_message[2];
}