/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  software
Filename: error_check_cycle.c
Purpose:  CSC data and initialization definitions
*/

/* CSC Includes */
#include <error_check.h>
#include <error_check_functions.h>

/* Library Includes */

/* Project Includes */

enum Common_status_code cycle_error_check(uint64_t cycle_count, void* arg) {
    enum Common_status_code return_code = COMMON_STATUS_OK;

    return return_code;
}
