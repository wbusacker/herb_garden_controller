/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  software
Filename: error_check_functions.h
Purpose:  CSC local function declarations
*/

#ifndef ERROR_CHECK_FUNCTIONS_H
#define ERROR_CHECK_FUNCTIONS_H

/* CSC Includes */
#include <error_check.h>
#include <error_check_data.h>

/* Library Includes */

/* Project Includes */

#endif
