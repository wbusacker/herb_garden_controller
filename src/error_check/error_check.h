/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  software
Filename: error_check.h
Purpose:  External CSC Header
*/

#ifndef ERROR_CHECK_H
#define ERROR_CHECK_H

/* CSC Includes */
#include <error_check_const.h>
#include <error_check_types.h>

/* Library Includes */

/* Project Includes */
#include <common.h>
#include <log.h>

enum Common_status_code init_error_check(void);
enum Common_status_code teardown_error_check(void);

enum Common_status_code cycle_error_check(uint64_t cycle_count, void* arg);

void error_check_backtrace_and_die(void);

#ifdef ENABLE_DEBUG

    #define ERROR_CHECK(condition, ...)                                                                                \
        if (condition) {                                                                                               \
            log_printf(LOG_LEVEL_ERROR, __FILE__, __LINE__, __VA_ARGS__);                                              \
            error_check_backtrace_and_die();                                                                           \
        }

    #define ERROR(...)                                                                                                 \
        log_printf(LOG_LEVEL_ERROR, __FILE__, __LINE__, __VA_ARGS__);                                                  \
        error_check_backtrace_and_die()

#else

    #define ERROR_CHECK(condition, ...)

#endif

#endif
