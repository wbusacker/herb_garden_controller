/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  tcars
Filename: common_template.c
Purpose:  Example Function File
*/

#include <error_check_functions.h>
#include <platform.h>

#define NUM_STACK_REGISTERS (31)

#define CONVERT_TO_HEX(x) ((x) > 9 ? (x) + 0x37 : (x) + 0x30)

#define SEND_SINGLE_CHAR(x)                                                                                            \
    {                                                                                                                  \
        TX1REG = x;                                                                                                    \
        while ((TX1STA & 0b10) == 0)                                                                                   \
            ;                                                                                                          \
    }

void error_check_backtrace_and_die(void) {

    INTCON = 0;

    SEND_SINGLE_CHAR('B')
    SEND_SINGLE_CHAR('A')
    SEND_SINGLE_CHAR('C')
    SEND_SINGLE_CHAR('K')
    SEND_SINGLE_CHAR('T')
    SEND_SINGLE_CHAR('R')
    SEND_SINGLE_CHAR('A')
    SEND_SINGLE_CHAR('C')
    SEND_SINGLE_CHAR('E')
    SEND_SINGLE_CHAR('_')
    SEND_SINGLE_CHAR('A')
    SEND_SINGLE_CHAR('N')
    SEND_SINGLE_CHAR('D')
    SEND_SINGLE_CHAR('_')
    SEND_SINGLE_CHAR('D')
    SEND_SINGLE_CHAR('I')
    SEND_SINGLE_CHAR('E')
    SEND_SINGLE_CHAR(':')
    SEND_SINGLE_CHAR('\n')

    uint8_t original_stack_pointer = STKPTR;

    for (uint8_t i = 0; i < original_stack_pointer; i++) {
        STKPTR = i;
        SEND_SINGLE_CHAR(CONVERT_TO_HEX(TOSU >> 4))
        SEND_SINGLE_CHAR(CONVERT_TO_HEX(TOSU & 0xF))

        SEND_SINGLE_CHAR(CONVERT_TO_HEX(TOSH >> 4))
        SEND_SINGLE_CHAR(CONVERT_TO_HEX(TOSH & 0xF))

        SEND_SINGLE_CHAR(CONVERT_TO_HEX(TOSL >> 4))
        SEND_SINGLE_CHAR(CONVERT_TO_HEX(TOSL & 0xF))
        SEND_SINGLE_CHAR('\n')
    }

    while (true)
        ;
}