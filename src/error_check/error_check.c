/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  software
Filename: error_check.c
Purpose:  CSC data and initialization definitions
*/

/* CSC Includes */
#include <error_check.h>
#include <error_check_functions.h>

/* Library Includes */

/* Project Includes */

enum Common_status_code init_error_check(void) {
    enum Common_status_code return_code = COMMON_STATUS_OK;

    return return_code;
}

enum Common_status_code teardown_error_check(void) {
    enum Common_status_code return_code = COMMON_STATUS_OK;

    return return_code;
}
