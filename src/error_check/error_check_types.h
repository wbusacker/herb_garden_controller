/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  software
Filename: error_check_types.h
Purpose:  CSC data types
*/

#ifndef ERROR_CHECK_TYPES_H
#define ERROR_CHECK_TYPES_H

/* CSC Includes */
#include <error_check_const.h>

/* Library Includes */

/* Project Includes */

#endif
