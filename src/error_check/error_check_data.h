/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  software
Filename: error_check_data.h
Purpose:  CSC data declaration
*/

#ifndef ERROR_CHECK_DATA_H
#define ERROR_CHECK_DATA_H

/* CSC Includes */
#include <error_check_const.h>
#include <error_check_types.h>

/* Library Includes */

/* Project Includes */

#endif
