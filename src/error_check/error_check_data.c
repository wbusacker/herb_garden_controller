/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  software
Filename: error_check_data.c
Purpose:  CSC data definition
*/

/* CSC Includes */
#include <error_check_data.h>

/* Library Includes */

/* Project Includes */
