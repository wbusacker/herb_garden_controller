/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  Herb Garden Controller
Filename: telemetry_data.h
Purpose:  CSC data declaration
*/

#ifndef TELEMETRY_DATA_H
#define TELEMETRY_DATA_H

/* CSC Includes */
#include <telemetry_autocode_tlm_ids.h>
#include <telemetry_const.h>
#include <telemetry_types.h>

/* Library Includes */

/* Project Includes */

extern uint8_t telemetry_buffer[TELEMETRY_AUTOCODE_MAX_BUFFER_LEN];

#endif
