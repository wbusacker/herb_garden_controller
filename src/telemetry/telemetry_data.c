/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  Herb Garden Controller
Filename: telemetry_data.c
Purpose:  CSC data definition
*/

/* CSC Includes */
#include <telemetry_data.h>

/* Library Includes */

/* Project Includes */

uint8_t telemetry_buffer[TELEMETRY_AUTOCODE_MAX_BUFFER_LEN];