/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  Herb Garden Controller
Filename: telemetry.h
Purpose:  External CSC Header
*/

#ifndef TELEMETRY_H
#define TELEMETRY_H

/* CSC Includes */
#include <telemetry_autocode_tlm_ids.h>
#include <telemetry_const.h>
#include <telemetry_types.h>

/* Library Includes */

/* Project Includes */
#include <common.h>

enum Common_status_code init_telemetry(void);
enum Common_status_code teardown_telemetry(void);

enum Common_status_code cycle_telemetry(uint64_t cycle_count, void* arg);

#endif
