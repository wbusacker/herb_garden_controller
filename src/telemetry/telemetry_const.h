/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  Herb Garden Controller
Filename: telemetry_const.h
Purpose:  CSC constants
*/

#ifndef TELEMETRY_CONST_H
#define TELEMETRY_CONST_H

/* CSC Includes */

/* Library Includes */

/* Project Includes */

#define TELEMETRY_HOLDING_BUFFER_LEN  (7)
#define TELEMETRY_TRANSMIT_BUFFER_LEN (8)
#define TELEMETRY_BIT_MASK            (0x80)
#define TELEMETRY_BIT_OVERFLOW_BYTE   (7)

#endif
