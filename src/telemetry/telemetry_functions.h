/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  Herb Garden Controller
Filename: telemetry_functions.h
Purpose:  CSC local function declarations
*/

#ifndef TELEMETRY_FUNCTIONS_H
#define TELEMETRY_FUNCTIONS_H

/* CSC Includes */
#include <telemetry.h>
#include <telemetry_autocode_tlm_ids.h>
#include <telemetry_data.h>

/* Library Includes */

/* Project Includes */

#endif
