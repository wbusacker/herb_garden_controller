/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  Herb Garden Controller
Filename: telemetry.c
Purpose:  CSC data and initialization definitions
*/

/* CSC Includes */
#include <telemetry.h>
#include <telemetry_functions.h>

/* Library Includes */

/* Project Includes */

enum Common_status_code init_telemetry() {
    enum Common_status_code return_code = COMMON_STATUS_OK;

    for (uint16_t i = 0; i < TELEMETRY_AUTOCODE_MAX_BUFFER_LEN; i++) {
        telemetry_buffer[i] = 0;
    }

    return return_code;
}

enum Common_status_code teardown_telemetry() {
    enum Common_status_code return_code = COMMON_STATUS_OK;

    return return_code;
}
