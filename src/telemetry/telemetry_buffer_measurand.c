/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  Herb Garden Controller
Filename: telemetry_template.c
Purpose:  Example Function File
*/

/* CSC Includes */
#include <telemetry_autocode_tlm_ids.h>
#include <telemetry_functions.h>

/* Library Includes */
#include <string.h>

/* Project Includes */
#include <log.h>

inline void telemetry_buffer_measurand(enum Telemetry_measurand_ids telemetry_id, void* data, uint8_t num_bytes) {
    uint16_t buffer_location = telemetry_autocode_buffer_locations[telemetry_id];
    memcpy(&(telemetry_buffer[buffer_location]), data, num_bytes);
}

void telemetry_buffer_measurand_uint8_t(enum Telemetry_measurand_ids telemetry_id, uint8_t data) {
    telemetry_buffer_measurand(telemetry_id, &data, 1);
}

void telemetry_buffer_measurand_uint16_t(enum Telemetry_measurand_ids telemetry_id, uint16_t data) {
    telemetry_buffer_measurand(telemetry_id, &data, 2);
}

void telemetry_buffer_measurand_uint32_t(enum Telemetry_measurand_ids telemetry_id, uint32_t data) {
    telemetry_buffer_measurand(telemetry_id, &data, 4);
}

void telemetry_buffer_measurand_uint64_t(enum Telemetry_measurand_ids telemetry_id, uint64_t data) {
    telemetry_buffer_measurand(telemetry_id, &data, 8);
}

void telemetry_buffer_measurand_int8_t(enum Telemetry_measurand_ids telemetry_id, int8_t data) {
    telemetry_buffer_measurand(telemetry_id, &data, 1);
}

void telemetry_buffer_measurand_int16_t(enum Telemetry_measurand_ids telemetry_id, int16_t data) {
    telemetry_buffer_measurand(telemetry_id, &data, 2);
}

void telemetry_buffer_measurand_int32_t(enum Telemetry_measurand_ids telemetry_id, int32_t data) {
    telemetry_buffer_measurand(telemetry_id, &data, 4);
}

void telemetry_buffer_measurand_int64_t(enum Telemetry_measurand_ids telemetry_id, int64_t data) {
    telemetry_buffer_measurand(telemetry_id, &data, 8);
}

void telemetry_buffer_measurand_float(enum Telemetry_measurand_ids telemetry_id, float data) {
    telemetry_buffer_measurand(telemetry_id, &data, 4);
}

void telemetry_buffer_measurand_double(enum Telemetry_measurand_ids telemetry_id, double data) {
    telemetry_buffer_measurand(telemetry_id, &data, 8);
}
