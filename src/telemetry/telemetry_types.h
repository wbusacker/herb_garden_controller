/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  Herb Garden Controller
Filename: telemetry_types.h
Purpose:  CSC data types
*/

#ifndef TELEMETRY_TYPES_H
#define TELEMETRY_TYPES_H

/* CSC Includes */
#include <telemetry_const.h>

/* Library Includes */

/* Project Includes */

#endif
