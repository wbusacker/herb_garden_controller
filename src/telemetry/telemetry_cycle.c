/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  Herb Garden Controller
Filename: telemetry_cycle.c
Purpose:  CSC data and initialization definitions
*/

/* CSC Includes */
#include <telemetry.h>
#include <telemetry_functions.h>

/* Library Includes */

/* Project Includes */
#include <uart.h>

enum Common_status_code cycle_telemetry(uint64_t cycle_count, void* arg) {
    enum Common_status_code return_code = COMMON_STATUS_OK;

    /* Pack 7 bytes of telemetry into 8 transmit bytes. Ensure the upper most bit of every
    sent byte is 1 so we don't accidentally use any ASCII control characters. */

    uint16_t tlm_buffer_index = 0;
    uint8_t  transmit_buffer[TELEMETRY_TRANSMIT_BUFFER_LEN];

    while (tlm_buffer_index < TELEMETRY_AUTOCODE_MAX_BUFFER_LEN) {

        transmit_buffer[TELEMETRY_BIT_OVERFLOW_BYTE] = TELEMETRY_BIT_MASK;

        /* Try to collect 7 bytes from the telemetry buffer for transmit */
        for (uint8_t i = 0; i < TELEMETRY_HOLDING_BUFFER_LEN; i++) {
            uint16_t fetch_index = (i + tlm_buffer_index);

            uint8_t fetched_data = 0;
            if (fetch_index < TELEMETRY_AUTOCODE_MAX_BUFFER_LEN) {
                fetched_data = telemetry_buffer[fetch_index];
            }

            transmit_buffer[i] = fetched_data | TELEMETRY_BIT_MASK;

            transmit_buffer[TELEMETRY_BIT_OVERFLOW_BYTE] |= (fetched_data & TELEMETRY_BIT_MASK) >> (i + 1);
        }

        uart_send_buffer(transmit_buffer, TELEMETRY_TRANSMIT_BUFFER_LEN);

        tlm_buffer_index += TELEMETRY_HOLDING_BUFFER_LEN;
    }

    uart_printf("\n");

    return return_code;
}
