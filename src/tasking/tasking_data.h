/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  Herb Garden Controller
Filename: tasking_data.h
Purpose:  CSC data declaration
*/

#ifndef TASKING_DATA_H
#define TASKING_DATA_H

/* CSC Includes */
#include <tasking_const.h>
#include <tasking_types.h>

/* Library Includes */
#include <stdint.h>

/* Project Includes */

extern uint16_t tasking_frequency_to_counts[TASKING_NUM_FREQUENCIES];
extern uint64_t tasking_main_cycle_count;

#endif
