/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  Herb Garden Controller
Filename: tasking_cycle.c
Purpose:  CSC data and initialization definitions
*/

/* CSC Includes */
#include <tasking.h>
#include <tasking_functions.h>

/* Library Includes */

/* Project Includes */
#include <log.h>
#include <rate_monotonic.h>
#include <telemetry.h>
#include <uptime.h>

enum Common_status_code tasking_cycle(struct Tasking_entry* tasks, uint16_t num_tasks) {
    enum Common_status_code return_code = COMMON_STATUS_OK;

    LOG_ALWAYS("Starting Periodic Processing\n");

    struct Rate_monotonic_t rm_control;

    rate_monotonic_init(&rm_control, TASKING_BASE_PERIOD_NS, RATE_MONOTONIC_IMMEDIATE_UNLOCK);

    bool     keep_cycling         = true;
    uint16_t hyper_period_counter = 0;

    uint16_t frequency_unlock_downcounter[TASKING_NUM_FREQUENCIES];

    for (uint8_t i = 0; i < TASKING_NUM_FREQUENCIES; i++) {
        frequency_unlock_downcounter[i] = tasking_frequency_to_counts[i];
    }

    for (uint16_t task = 0; task < num_tasks; task++) {
        tasks[task].minimum_cycle_execution_time_ns = 0xFFFFFFFFFFFFFFFF;
    }

    while (keep_cycling) {
        rate_monotonic_cycle(&rm_control);

        /* Decrement all down counters for frequency selection */
        for (uint8_t i = 0; i < TASKING_NUM_FREQUENCIES; i++) {
            frequency_unlock_downcounter[i]--;
        }

        for (uint16_t task = 0; task < num_tasks; task++) {

            if (frequency_unlock_downcounter[tasks[task].frequency] == 0) {

                uint64_t                time_start = uptime_nanoseconds();
                enum Common_status_code status = tasks[task].function(tasking_main_cycle_count, tasks[task].arguments);
                uint64_t                time_stop  = uptime_nanoseconds();
                uint64_t                delta_time = time_stop - time_start;

                tasks[task].cycle_count++;
                tasks[task].total_cycle_execution_time_ns += delta_time;

                if (delta_time > tasks[task].maximum_cycle_execution_time_ns) {
                    tasks[task].maximum_cycle_execution_time_ns = delta_time;
                }

                if (delta_time < tasks[task].minimum_cycle_execution_time_ns) {
                    tasks[task].minimum_cycle_execution_time_ns = delta_time;
                }

                if (status != COMMON_STATUS_OK) {
                    keep_cycling = false;
                }

                telemetry_buffer_measurand_uint64_t(tasks[task].min_runtime_tlm_id,
                                                    tasks[task].minimum_cycle_execution_time_ns);
                telemetry_buffer_measurand_uint64_t(tasks[task].max_runtime_tlm_id,
                                                    tasks[task].maximum_cycle_execution_time_ns);
                telemetry_buffer_measurand_uint64_t(tasks[task].ave_runtime_tlm_id,
                                                    tasks[task].total_cycle_execution_time_ns /
                                                      tasks[task].cycle_count);
            }
        }

        /* If any of the down counters hit zero, reset them */
        for (uint8_t i = 0; i < TASKING_NUM_FREQUENCIES; i++) {
            if (frequency_unlock_downcounter[i] == 0) {
                frequency_unlock_downcounter[i] = tasking_frequency_to_counts[i];
            }
        }

        int64_t min;
        int64_t max;
        int64_t ave;
        int64_t missed;

        rate_monotonic_get_statistics(&rm_control, &ave, &min, &max, &missed);

        ave /= (TASKING_BASE_PERIOD_NS / 10000);

        int16_t reduced_ave = (int16_t)ave;

        TELEMETRY_CPU_UTILIZATION_BUFFER(reduced_ave);

        if (hyper_period_counter == tasking_frequency_to_counts[TASKING_NUM_FREQUENCIES - 1]) {
            hyper_period_counter = (uint16_t)-1;
        }

        tasking_main_cycle_count++;
        hyper_period_counter++;
    }

    return return_code;
}
