/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  Herb Garden Controller
Filename: tasking_data.c
Purpose:  CSC data definition
*/

/* CSC Includes */
#include <tasking_data.h>

/* Library Includes */

/* Project Includes */

uint16_t tasking_frequency_to_counts[TASKING_NUM_FREQUENCIES] = {
  1,     /* TASKING_FREQUENCY_10_HZ     0.1  Second  */
  2,     /* TASKING_FREQUENCY_5_HZ      0.2  Second  */
  5,     /* TASKING_FREQUENCY_2_HZ      0.5  Second  */
  10,    /* TASKING_FREQUENCY_1_HZ      1    Second  */
  20,    /* TASKING_FREQUENCY_1_2_HZ    2    Seconds */
  100,   /* TASKING_FREQUENCY_1_10_HZ   10   Seconds */
  300,   /* TASKING_FREQUENCY_1_30_HZ   30   Seconds */
  600,   /* TASKING_FREQUENCY_1_60_HZ   1    Minute  */
  3000,  /* TASKING_FREQUENCY_1_300_HZ  5    Minutes */
  18000, /* TASKING_FREQUENCY_1_1800_HZ 30   Minutes */
  36000  /* TASKING_FREQUENCY_1_3600_HZ 1    Hour    */
};

uint64_t tasking_main_cycle_count;