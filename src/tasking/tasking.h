/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  Herb Garden Controller
Filename: tasking.h
Purpose:  External CSC Header
*/

#ifndef TASKING_H
#define TASKING_H

/* CSC Includes */
#include <tasking_const.h>
#include <tasking_types.h>

/* Library Includes */

/* Project Includes */
#include <common.h>

enum Common_status_code init_tasking(void);
enum Common_status_code teardown_tasking(void);

enum Common_status_code tasking_cycle(struct Tasking_entry* tasks, uint16_t num_tasks);

#endif
