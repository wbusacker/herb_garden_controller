/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  Herb Garden Controller
Filename: tasking_functions.h
Purpose:  CSC local function declarations
*/

#ifndef TASKING_FUNCTIONS_H
#define TASKING_FUNCTIONS_H

/* CSC Includes */
#include <tasking.h>
#include <tasking_data.h>

/* Library Includes */

/* Project Includes */

#endif
