/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  Herb Garden Controller
Filename: tasking.c
Purpose:  CSC data and initialization definitions
*/

/* CSC Includes */
#include <tasking.h>
#include <tasking_functions.h>

/* Library Includes */

/* Project Includes */
#include <telemetry.h>

enum Common_status_code init_tasking(void) {
    enum Common_status_code return_code = COMMON_STATUS_OK;

    tasking_main_cycle_count = 0;

    return return_code;
}

enum Common_status_code teardown_tasking(void) {
    enum Common_status_code return_code = COMMON_STATUS_OK;

    return return_code;
}
