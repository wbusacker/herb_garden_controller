/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  software
Filename: pic18_io_functions.h
Purpose:  CSC local function declarations
*/

#ifndef PIC18_IO_FUNCTIONS_H
#define PIC18_IO_FUNCTIONS_H

/* CSC Includes */
#include <pic18_io.h>
#include <pic18_io_data.h>

/* Library Includes */

/* Project Includes */

void pic18_io_set_direction(enum PIC18_io_pin pin, enum IO_pin_direction direction);

#endif
