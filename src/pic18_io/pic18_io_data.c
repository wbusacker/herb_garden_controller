/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  software
Filename: pic18_io_data.c
Purpose:  CSC data definition
*/

/* CSC Includes */
#include <pic18_io_data.h>

/* Library Includes */

/* Project Includes */
#include <platform.h>

// clang-format off
const struct PIC18_io_pin_definition pic18_io_pin_definitions[PIC18_IO_PIN_COUNT] = {
    [PIC18_IO_PIN_2]  = {.port = PIC18_IO_PORT_A, .discrete_bit = 0, .pps_output_register = &RA0PPS, .pps_input_number = 0b00000},
    [PIC18_IO_PIN_3]  = {.port = PIC18_IO_PORT_A, .discrete_bit = 1, .pps_output_register = &RA1PPS, .pps_input_number = 0b00001},
    [PIC18_IO_PIN_4]  = {.port = PIC18_IO_PORT_A, .discrete_bit = 2, .pps_output_register = &RA2PPS, .pps_input_number = 0b00010},
    [PIC18_IO_PIN_5]  = {.port = PIC18_IO_PORT_A, .discrete_bit = 3, .pps_output_register = &RA3PPS, .pps_input_number = 0b00011},
    [PIC18_IO_PIN_6]  = {.port = PIC18_IO_PORT_A, .discrete_bit = 4, .pps_output_register = &RA4PPS, .pps_input_number = 0b00100},
    [PIC18_IO_PIN_7]  = {.port = PIC18_IO_PORT_A, .discrete_bit = 5, .pps_output_register = &RA5PPS, .pps_input_number = 0b00101},
    [PIC18_IO_PIN_8]  = {.port = PIC18_IO_PORT_E, .discrete_bit = 0, .pps_output_register = &RE0PPS, .pps_input_number = 0b00000},
    [PIC18_IO_PIN_9]  = {.port = PIC18_IO_PORT_E, .discrete_bit = 1, .pps_output_register = &RE1PPS, .pps_input_number = 0b00001},
    [PIC18_IO_PIN_10] = {.port = PIC18_IO_PORT_E, .discrete_bit = 2, .pps_output_register = &RE2PPS, .pps_input_number = 0b00010},
    [PIC18_IO_PIN_15] = {.port = PIC18_IO_PORT_C, .discrete_bit = 0, .pps_output_register = &RC0PPS, .pps_input_number = 0b10000},
    [PIC18_IO_PIN_16] = {.port = PIC18_IO_PORT_C, .discrete_bit = 1, .pps_output_register = &RC1PPS, .pps_input_number = 0b10001},
    [PIC18_IO_PIN_17] = {.port = PIC18_IO_PORT_C, .discrete_bit = 2, .pps_output_register = &RC2PPS, .pps_input_number = 0b10010},
    [PIC18_IO_PIN_18] = {.port = PIC18_IO_PORT_C, .discrete_bit = 3, .pps_output_register = &RC3PPS, .pps_input_number = 0b10011},
    [PIC18_IO_PIN_19] = {.port = PIC18_IO_PORT_D, .discrete_bit = 0, .pps_output_register = &RD0PPS, .pps_input_number = 0b11000},
    [PIC18_IO_PIN_20] = {.port = PIC18_IO_PORT_D, .discrete_bit = 1, .pps_output_register = &RD1PPS, .pps_input_number = 0b11001},
    [PIC18_IO_PIN_21] = {.port = PIC18_IO_PORT_D, .discrete_bit = 2, .pps_output_register = &RD2PPS, .pps_input_number = 0b11010},
    [PIC18_IO_PIN_22] = {.port = PIC18_IO_PORT_D, .discrete_bit = 3, .pps_output_register = &RD3PPS, .pps_input_number = 0b11011},
    [PIC18_IO_PIN_23] = {.port = PIC18_IO_PORT_C, .discrete_bit = 4, .pps_output_register = &RC4PPS, .pps_input_number = 0b10100},
    [PIC18_IO_PIN_24] = {.port = PIC18_IO_PORT_C, .discrete_bit = 5, .pps_output_register = &RC5PPS, .pps_input_number = 0b10101},
    [PIC18_IO_PIN_25] = {.port = PIC18_IO_PORT_C, .discrete_bit = 6, .pps_output_register = &RC6PPS, .pps_input_number = 0b10110},
    [PIC18_IO_PIN_26] = {.port = PIC18_IO_PORT_C, .discrete_bit = 7, .pps_output_register = &RC7PPS, .pps_input_number = 0b10110},
    [PIC18_IO_PIN_27] = {.port = PIC18_IO_PORT_D, .discrete_bit = 4, .pps_output_register = &RD4PPS, .pps_input_number = 0b11100},
    [PIC18_IO_PIN_28] = {.port = PIC18_IO_PORT_D, .discrete_bit = 5, .pps_output_register = &RD5PPS, .pps_input_number = 0b11101},
    [PIC18_IO_PIN_29] = {.port = PIC18_IO_PORT_D, .discrete_bit = 6, .pps_output_register = &RD6PPS, .pps_input_number = 0b11110},
    [PIC18_IO_PIN_30] = {.port = PIC18_IO_PORT_D, .discrete_bit = 7, .pps_output_register = &RD7PPS, .pps_input_number = 0b11110},
    [PIC18_IO_PIN_33] = {.port = PIC18_IO_PORT_B, .discrete_bit = 0, .pps_output_register = &RB0PPS, .pps_input_number = 0b01000},
    [PIC18_IO_PIN_34] = {.port = PIC18_IO_PORT_B, .discrete_bit = 1, .pps_output_register = &RB1PPS, .pps_input_number = 0b01001},
    [PIC18_IO_PIN_35] = {.port = PIC18_IO_PORT_B, .discrete_bit = 2, .pps_output_register = &RB2PPS, .pps_input_number = 0b01010},
    [PIC18_IO_PIN_36] = {.port = PIC18_IO_PORT_B, .discrete_bit = 3, .pps_output_register = &RB3PPS, .pps_input_number = 0b01011},
    [PIC18_IO_PIN_37] = {.port = PIC18_IO_PORT_B, .discrete_bit = 4, .pps_output_register = &RB4PPS, .pps_input_number = 0b01100},
    [PIC18_IO_PIN_38] = {.port = PIC18_IO_PORT_B, .discrete_bit = 5, .pps_output_register = &RB5PPS, .pps_input_number = 0b01101}
};

const struct PIC18_io_port_control_register_defintion pic18_io_port_control_register_defintions[PIC18_IO_PORT_COUNT] = {
    [PIC18_IO_PORT_A] = {.direction = &TRISA, .port = &PORTA, .latch = &LATA, .analog_select = &ANSELA, .pullup = &WPUA, .input_level = &INLVLA, .slew_rate = &SLRCONA, .open_drain = &ODCONA},
    [PIC18_IO_PORT_B] = {.direction = &TRISB, .port = &PORTB, .latch = &LATB, .analog_select = &ANSELB, .pullup = &WPUB, .input_level = &INLVLB, .slew_rate = &SLRCONB, .open_drain = &ODCONB},
    [PIC18_IO_PORT_C] = {.direction = &TRISC, .port = &PORTC, .latch = &LATC, .analog_select = &ANSELC, .pullup = &WPUC, .input_level = &INLVLC, .slew_rate = &SLRCONC, .open_drain = &ODCONC},
    [PIC18_IO_PORT_D] = {.direction = &TRISD, .port = &PORTD, .latch = &LATD, .analog_select = &ANSELD, .pullup = &WPUD, .input_level = &INLVLD, .slew_rate = &SLRCOND, .open_drain = &ODCOND},
    [PIC18_IO_PORT_E] = {.direction = &TRISE, .port = &PORTE, .latch = &LATE, .analog_select = &ANSELE, .pullup = &WPUE, .input_level = &INLVLE, .slew_rate = &SLRCONE, .open_drain = &ODCONE},
};

const struct PIC18_io_pin_function_definition pic18_io_pin_function_definitions[PIC18_IO_PIN_FUNCTION_COUNT] = {                                                                        /* A      B      C      D      E    */
    [PIC18_IO_PIN_FUNCTION_DISCRETE_OUT]      = {.mode = PIC18_IO_PIN_MODE_DISCRETE,          .definition = {.discrete = IO_OUT},                            .allowable_ports = {true,  true,  true,  true,  true }},
    [PIC18_IO_PIN_FUNCTION_DISCRETE_IN]       = {.mode = PIC18_IO_PIN_MODE_DISCRETE,          .definition = {.discrete = IO_IN},                             .allowable_ports = {true,  true,  true,  true,  true }},
    [PIC18_IO_PIN_FUNCTION_UART_1_RX]         = {.mode = PIC18_IO_PIN_MODE_PERIPHERAL_INPUT,  .definition = {.input  = {.pps_input_register = &RX1PPS}},     .allowable_ports = {false, true,  true,  false, false}},
    [PIC18_IO_PIN_FUNCTION_UART_1_TX]         = {.mode = PIC18_IO_PIN_MODE_PERIPHERAL_OUTPUT, .definition = {.output = {.function_id = 0x09}},               .allowable_ports = {false, true,  true,  false, false}},
    [PIC18_IO_PIN_FUNCTION_UART_2_RX]         = {.mode = PIC18_IO_PIN_MODE_PERIPHERAL_INPUT,  .definition = {.input  = {.pps_input_register = &RX2PPS}},     .allowable_ports = {false, true,  false, true,  false}},
    [PIC18_IO_PIN_FUNCTION_UART_2_TX]         = {.mode = PIC18_IO_PIN_MODE_PERIPHERAL_OUTPUT, .definition = {.output = {.function_id = 0x0B}},               .allowable_ports = {false, true,  false, true,  false}},
    [PIC18_IO_PIN_FUNCTION_SPI_1_MISO]        = {.mode = PIC18_IO_PIN_MODE_PERIPHERAL_INPUT,  .definition = {.input  = {.pps_input_register = &SSP1DATPPS}}, .allowable_ports = {false, true,  true,  false, false}},
    [PIC18_IO_PIN_FUNCTION_SPI_1_MOSI]        = {.mode = PIC18_IO_PIN_MODE_PERIPHERAL_OUTPUT, .definition = {.output = {.function_id = 0x10}},               .allowable_ports = {false, true,  true,  false, false}},
    [PIC18_IO_PIN_FUNCTION_SPI_1_CLOCK_OUT]   = {.mode = PIC18_IO_PIN_MODE_PERIPHERAL_OUTPUT, .definition = {.output = {.function_id = 0x0F}},               .allowable_ports = {false, true,  true,  false, false}},
    [PIC18_IO_PIN_FUNCTION_SPI_1_CLOCK_IN]    = {.mode = PIC18_IO_PIN_MODE_PERIPHERAL_INPUT,  .definition = {.input  = {.pps_input_register = &SSP1CLKPPS}}, .allowable_ports = {false, true,  true,  false, false}},
    [PIC18_IO_PIN_FUNCTION_SPI_1_CHIP_SELECT] = {.mode = PIC18_IO_PIN_MODE_PERIPHERAL_INPUT,  .definition = {.input  = {.pps_input_register = &SSP1SSPPS}},  .allowable_ports = {true,  false, false, true,  false}},
    [PIC18_IO_PIN_FUNCTION_SPI_2_MISO]        = {.mode = PIC18_IO_PIN_MODE_PERIPHERAL_INPUT,  .definition = {.input  = {.pps_input_register = &SSP2DATPPS}}, .allowable_ports = {false, true,  false, true,  false}},
    [PIC18_IO_PIN_FUNCTION_SPI_2_MOSI]        = {.mode = PIC18_IO_PIN_MODE_PERIPHERAL_OUTPUT, .definition = {.output = {.function_id = 0x12}},               .allowable_ports = {false, true,  false, true,  false}},
    [PIC18_IO_PIN_FUNCTION_SPI_2_CLOCK_OUT]   = {.mode = PIC18_IO_PIN_MODE_PERIPHERAL_OUTPUT, .definition = {.output = {.function_id = 0x11}},               .allowable_ports = {false, true,  false, true,  false}},
    [PIC18_IO_PIN_FUNCTION_SPI_2_CLOCK_IN]    = {.mode = PIC18_IO_PIN_MODE_PERIPHERAL_INPUT,  .definition = {.input  = {.pps_input_register = &SSP2CLKPPS}}, .allowable_ports = {false, true,  false, true,  false}},
    [PIC18_IO_PIN_FUNCTION_SPI_2_CHIP_SELECT] = {.mode = PIC18_IO_PIN_MODE_PERIPHERAL_INPUT,  .definition = {.input  = {.pps_input_register = &SSP2SSPPS}},  .allowable_ports = {false, true,  false, true,  false}},
};

// clang-format on