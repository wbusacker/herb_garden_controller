/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  software
Filename: pic18_io_const.h
Purpose:  CSC constants
*/

#ifndef PIC18_IO_CONST_H
#define PIC18_IO_CONST_H

/* CSC Includes */
#include <pic18_io_allocation.h>

/* Library Includes */

/* Project Includes */

#define PIC18_IO_PERIPERHAL_INPUT_PORT_MASK      (0b11)
#define PIC18_IO_PERIPERHAL_INPUT_PORT_BIT_SHIFT (3)
#define PIC18_IO_PERIPERHAL_INPUT_BIT_MASK       (0b111)
#define PIC18_IO_PERIPERHAL_INPUT_BIT_BIT_SHIFT  (0)

enum PIC18_io_port {
    PIC18_IO_PORT_A = 0,
    PIC18_IO_PORT_B = 1,
    PIC18_IO_PORT_C = 2,
    PIC18_IO_PORT_D = 3,
    PIC18_IO_PORT_E = 4,
    PIC18_IO_PORT_COUNT
};

enum PIC18_io_pin_mode {
    PIC18_IO_PIN_MODE_PERIPHERAL_OUTPUT,
    PIC18_IO_PIN_MODE_PERIPHERAL_INPUT,
    PIC18_IO_PIN_MODE_DISCRETE
};

#define PIC18_PORT_NONE (PIC18_PORT_COUNT)

enum PIC18_io_pin_function {
    PIC18_IO_PIN_FUNCTION_DISCRETE_OUT,
    PIC18_IO_PIN_FUNCTION_DISCRETE_IN,
    PIC18_IO_PIN_FUNCTION_UART_1_RX,
    PIC18_IO_PIN_FUNCTION_UART_1_TX,
    PIC18_IO_PIN_FUNCTION_UART_2_RX,
    PIC18_IO_PIN_FUNCTION_UART_2_TX,
    PIC18_IO_PIN_FUNCTION_SPI_1_MISO,
    PIC18_IO_PIN_FUNCTION_SPI_1_MOSI,
    PIC18_IO_PIN_FUNCTION_SPI_1_CLOCK_OUT,
    PIC18_IO_PIN_FUNCTION_SPI_1_CLOCK_IN,
    PIC18_IO_PIN_FUNCTION_SPI_1_CHIP_SELECT,
    PIC18_IO_PIN_FUNCTION_SPI_2_MISO,
    PIC18_IO_PIN_FUNCTION_SPI_2_MOSI,
    PIC18_IO_PIN_FUNCTION_SPI_2_CLOCK_IN,
    PIC18_IO_PIN_FUNCTION_SPI_2_CLOCK_OUT,
    PIC18_IO_PIN_FUNCTION_SPI_2_CHIP_SELECT,
    PIC18_IO_PIN_FUNCTION_COUNT
};

#endif
