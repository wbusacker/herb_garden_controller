/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  software
Filename: pic18_io_types.h
Purpose:  CSC data types
*/

#ifndef PIC18_IO_TYPES_H
#define PIC18_IO_TYPES_H

/* CSC Includes */
#include <pic18_io_const.h>

/* Library Includes */
#include <stdbool.h>
#include <stdint.h>

/* Project Includes */
#include <io_common_const.h>

struct PIC18_io_port_control_register_defintion {
    volatile uint8_t* direction;
    volatile uint8_t* port;
    volatile uint8_t* latch;
    volatile uint8_t* analog_select;
    volatile uint8_t* pullup;
    volatile uint8_t* input_level;
    volatile uint8_t* slew_rate;
    volatile uint8_t* open_drain;
};

struct PIC18_io_pin_definition {
    enum PIC18_io_port port;
    uint8_t            discrete_bit;
    volatile uint8_t*  pps_output_register;
    uint8_t            pps_input_number;
};

struct PIC18_io_peripheral_input_definition {
    volatile uint8_t* pps_input_register;
};

struct PIC18_io_peripheral_output_definition {
    uint8_t function_id;
};

union PIC18_io_peripheral_io_definition {
    struct PIC18_io_peripheral_input_definition  input;
    struct PIC18_io_peripheral_output_definition output;
    enum IO_pin_direction                        discrete;
};

struct PIC18_io_pin_function_definition {
    enum PIC18_io_pin_mode                  mode;
    union PIC18_io_peripheral_io_definition definition;
    bool                                    allowable_ports[PIC18_IO_PORT_COUNT];
};

#endif
