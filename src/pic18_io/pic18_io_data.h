/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  software
Filename: pic18_io_data.h
Purpose:  CSC data declaration
*/

#ifndef PIC18_IO_DATA_H
#define PIC18_IO_DATA_H

/* CSC Includes */
#include <pic18_io_const.h>
#include <pic18_io_types.h>

/* Library Includes */

/* Project Includes */

extern const struct PIC18_io_pin_definition pic18_io_pin_definitions[PIC18_IO_PIN_COUNT];
extern const struct PIC18_io_port_control_register_defintion
                                                     pic18_io_port_control_register_defintions[PIC18_IO_PORT_COUNT];
extern const struct PIC18_io_pin_function_definition pic18_io_pin_function_definitions[PIC18_IO_PIN_FUNCTION_COUNT];

#endif
