/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  software
Filename: pic18_io.h
Purpose:  External CSC Header
*/

#ifndef PIC18_IO_H
#define PIC18_IO_H

/* CSC Includes */
#include <pic18_io_const.h>
#include <pic18_io_types.h>

/* Library Includes */

/* Project Includes */
#include <common.h>
#include <io_common_const.h>

enum Common_status_code init_pic18_io(void);
enum Common_status_code teardown_pic18_io(void);

enum Common_status_code cycle_pic18_io(uint64_t cycle_count, void* arg);

void                pic18_io_set(enum PIC18_io_pin pin, enum IO_logic_state state);
enum IO_logic_state pic18_io_read(enum PIC18_io_pin pin);
void                pic18_io_set_function(enum PIC18_io_pin pin, enum PIC18_io_pin_function function);

#endif
