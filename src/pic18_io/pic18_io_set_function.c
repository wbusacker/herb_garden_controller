/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  software
Filename: pic18_io_template.c
Purpose:  Example Function File
*/

/* CSC Includes */
#include <pic18_io_functions.h>

/* Library Includes */

/* Project Includes */
#include <error_check.h>
#include <log.h>

void pic18_io_set_function(enum PIC18_io_pin pin, enum PIC18_io_pin_function function) {

    ERROR_CHECK(pin >= PIC18_IO_PIN_COUNT, "Invalid pin %d\n", pin);
    ERROR_CHECK(! pic18_io_pin_function_definitions[function].allowable_ports[pic18_io_pin_definitions[pin].port],
                "Function %d is not allowed on pin %d\n",
                function,
                pin)

    switch (pic18_io_pin_function_definitions[function].mode) {
        case PIC18_IO_PIN_MODE_PERIPHERAL_INPUT:
            *(pic18_io_pin_function_definitions[function].definition.input.pps_input_register) =
              pic18_io_pin_definitions[pin].pps_input_number;
            pic18_io_set_direction(pin, IO_IN);
            LOG_DEBUG("PPS Input Addr %04X set to %d\n",
                      (uint16_t)pic18_io_pin_function_definitions[function].definition.input.pps_input_register,
                      pic18_io_pin_definitions[pin].pps_input_number);
            break;
        case PIC18_IO_PIN_MODE_PERIPHERAL_OUTPUT:
            *(pic18_io_pin_definitions[pin].pps_output_register) =
              pic18_io_pin_function_definitions[function].definition.output.function_id;
            pic18_io_set_direction(pin, IO_OUT);
            LOG_DEBUG("PPS Output Addr %04X set to %d\n",
                      (uint16_t)pic18_io_pin_definitions[pin].pps_output_register,
                      pic18_io_pin_function_definitions[function].definition.output.function_id);
            break;
        case PIC18_IO_PIN_MODE_DISCRETE:
            pic18_io_set_direction(pin, pic18_io_pin_function_definitions[function].definition.discrete);
            break;
        default:
            ERROR("Unknown Pin Function Mode, PIC18 Pin %d PIC18 Function %d\n", pin, function);
            break;
    }
}