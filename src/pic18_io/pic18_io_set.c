/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  software
Filename: pic18_io_template.c
Purpose:  Example Function File
*/

/* CSC Includes */
#include <pic18_io_functions.h>

/* Library Includes */

/* Project Includes */
#include <error_check.h>
#include <log.h>

void pic18_io_set(enum PIC18_io_pin pin, enum IO_logic_state state) {

    ERROR_CHECK(pin >= PIC18_IO_PIN_COUNT, "Invalid pin %d\n", pin);

    uint8_t data = *(pic18_io_port_control_register_defintions[pic18_io_pin_definitions[pin].port].latch);
    uint8_t bit  = (uint8_t)(1 << pic18_io_pin_definitions[pin].discrete_bit);

    if (state == IO_LOGIC_HIGH) {
        data |= bit;
    } else {
        data &= ~bit;
    }

    *(pic18_io_port_control_register_defintions[pic18_io_pin_definitions[pin].port].latch) = data;
}