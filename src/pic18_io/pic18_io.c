/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  software
Filename: pic18_io.c
Purpose:  CSC data and initialization definitions
*/

/* CSC Includes */
#include <pic18_io.h>
#include <pic18_io_functions.h>

/* Library Includes */
#include <stddef.h>

/* Project Includes */

enum Common_status_code init_pic18_io(void) {
    enum Common_status_code return_code = COMMON_STATUS_OK;

    for (uint8_t port = 0; port < PIC18_IO_PORT_COUNT; port++) {
        *(pic18_io_port_control_register_defintions[port].direction)     = 0xFF;
        *(pic18_io_port_control_register_defintions[port].port)          = 0x00;
        *(pic18_io_port_control_register_defintions[port].latch)         = 0x00;
        *(pic18_io_port_control_register_defintions[port].analog_select) = 0x00;
        *(pic18_io_port_control_register_defintions[port].pullup)        = 0x00;
        *(pic18_io_port_control_register_defintions[port].input_level)   = 0x00;
        *(pic18_io_port_control_register_defintions[port].slew_rate)     = 0x00;
        *(pic18_io_port_control_register_defintions[port].open_drain)    = 0x00;
    }

    for (uint8_t pin = 0; pin < PIC18_IO_PIN_COUNT; pin++) {
        if (pic18_io_pin_definitions[pin].pps_output_register != NULL) {
            *(pic18_io_pin_definitions[pin].pps_output_register) = 0;
        }
    }

    return return_code;
}

enum Common_status_code teardown_pic18_io(void) {
    enum Common_status_code return_code = COMMON_STATUS_OK;

    return return_code;
}
