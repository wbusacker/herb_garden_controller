/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  software
Filename: pic18_io_const.h
Purpose:  CSC constants
*/

#ifndef PIC18_IO_ALLOCATION_H
#define PIC18_IO_ALLOCATION_H

/* CSC Includes */

/* Library Includes */

/* Project Includes */

enum PIC18_io_pin {
    // PIC18_IO_PIN_1,
    PIC18_IO_PIN_2,
    PIC18_IO_PIN_3,
    PIC18_IO_PIN_4,
    PIC18_IO_PIN_5,
    PIC18_IO_PIN_6,
    PIC18_IO_PIN_7,
    PIC18_IO_PIN_8,
    PIC18_IO_PIN_9,
    PIC18_IO_PIN_10,
    // PIC18_IO_PIN_11,
    // PIC18_IO_PIN_12,
    // PIC18_IO_PIN_13,
    // PIC18_IO_PIN_14,
    PIC18_IO_PIN_15,
    PIC18_IO_PIN_16,
    PIC18_IO_PIN_17,
    PIC18_IO_PIN_18,
    PIC18_IO_PIN_19,
    PIC18_IO_PIN_20,
    PIC18_IO_PIN_21,
    PIC18_IO_PIN_22,
    PIC18_IO_PIN_23,
    PIC18_IO_PIN_24,
    PIC18_IO_PIN_25,
    PIC18_IO_PIN_26,
    PIC18_IO_PIN_27,
    PIC18_IO_PIN_28,
    PIC18_IO_PIN_29,
    PIC18_IO_PIN_30,
    // PIC18_IO_PIN_31,
    // PIC18_IO_PIN_32,
    PIC18_IO_PIN_33,
    PIC18_IO_PIN_34,
    PIC18_IO_PIN_35,
    PIC18_IO_PIN_36,
    PIC18_IO_PIN_37,
    PIC18_IO_PIN_38,
    // PIC18_IO_PIN_39,
    // PIC18_IO_PIN_40,
    PIC18_IO_PIN_COUNT
};

#endif
