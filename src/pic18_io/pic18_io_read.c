/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  software
Filename: pic18_io_template.c
Purpose:  Example Function File
*/

/* CSC Includes */
#include <pic18_io_functions.h>

/* Library Includes */

/* Project Includes */
#include <error_check.h>
#include <log.h>

enum IO_logic_state pic18_io_read(enum PIC18_io_pin pin) {

    ERROR_CHECK(pin >= PIC18_IO_PIN_COUNT, "Invalid pin %d\n", pin);

    uint8_t data = *(pic18_io_port_control_register_defintions[pic18_io_pin_definitions[pin].port].port);
    uint8_t bit  = (data >> pic18_io_pin_definitions[pin].discrete_bit) & 1;

    return bit == 0 ? IO_LOGIC_LOW : IO_LOGIC_HIGH;
}