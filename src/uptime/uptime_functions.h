/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  Herb Garden Controller
Filename: uptime_functions.h
Purpose:  CSC local function declarations
*/

#ifndef UPTIME_FUNCTIONS_H
#define UPTIME_FUNCTIONS_H

/* CSC Includes */
#include <uptime.h>
#include <uptime_data.h>

/* Library Includes */

/* Project Includes */

#endif
