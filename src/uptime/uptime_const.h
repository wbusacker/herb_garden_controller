/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  Herb Garden Controller
Filename: uptime_const.h
Purpose:  CSC constants
*/

#ifndef UPTIME_CONST_H
#define UPTIME_CONST_H

/* CSC Includes */

/* Library Includes */

/* Project Includes */
#include <common.h>

#define MS_PER_S (1000ll)

#define US_PER_S  (1000000ll)
#define US_PER_MS (1000ll)

#define NS_PER_S  (1000000000ll)
#define NS_PER_MS (1000000ll)
#define NS_PER_US (1000ll)

#define UPTIME_MS_PER_TICK (1ll)
#define UPTIME_NS_PER_TICK (1000000ll)

#define UPTIME_TIMER_RESET     (250)
#define UPTIME_TIMER_THRESHOLD (248)

#endif
