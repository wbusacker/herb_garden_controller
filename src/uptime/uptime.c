/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  Herb Garden Controller
Filename: uptime.c
Purpose:  CSC data and initialization definitions
*/

/* CSC Includes */
#include <uptime.h>
#include <uptime_functions.h>

/* Library Includes */

/* Project Includes */
#include <platform.h>

enum Common_status_code init_uptime(void) {
    enum Common_status_code return_code = COMMON_STATUS_OK;

    uptime_milliseconds_counter = 0;

    /* Configure Timer 1 to go off every millisecond */
    TMR0IE = 1;

    T0CON0 = 0b00000000;
    T0CON1 = 0b01010100;
    TMR0H  = 250;
    TMR0L  = 0;
    T0CON0 = 0b10000000;

    return return_code;
}

enum Common_status_code teardown_uptime(void) {
    enum Common_status_code return_code = COMMON_STATUS_OK;

    return return_code;
}
