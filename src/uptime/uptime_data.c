/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  Herb Garden Controller
Filename: uptime_data.c
Purpose:  CSC data definition
*/

/* CSC Includes */
#include <uptime_data.h>

/* Library Includes */

/* Project Includes */

volatile uint64_t uptime_milliseconds_counter = 0;