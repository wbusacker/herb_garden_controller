/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  Herb Garden Controller
Filename: uptime.h
Purpose:  External CSC Header
*/

#ifndef UPTIME_H
#define UPTIME_H

/* CSC Includes */
#include <uptime_const.h>
#include <uptime_types.h>

/* Library Includes */

/* Project Includes */
#include <common.h>

enum Common_status_code init_uptime(void);
enum Common_status_code teardown_uptime(void);

uint64_t uptime_nanoseconds(void);
uint64_t uptime_milliseconds(void);

void uptime_interrupt(void);

#endif
