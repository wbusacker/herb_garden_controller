/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  Herb Garden Controller
Filename: uptime_types.h
Purpose:  CSC data types
*/

#ifndef UPTIME_TYPES_H
#define UPTIME_TYPES_H

/* CSC Includes */
#include <uptime_const.h>

/* Library Includes */

/* Project Includes */

#endif
