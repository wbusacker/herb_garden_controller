/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  Herb Garden Controller
Filename: uptime_template.c
Purpose:  Example Function File
*/

/* CSC Includes */
#include <uptime_functions.h>

/* Library Includes */

/* Project Includes */

uint64_t uptime_milliseconds(void) {
    return uptime_milliseconds_counter;
}
