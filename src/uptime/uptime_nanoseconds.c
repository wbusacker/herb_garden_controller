/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  Herb Garden Controller
Filename: uptime_template.c
Purpose:  Example Function File
*/

/* CSC Includes */
#include <uptime_functions.h>

/* Library Includes */

/* Project Includes */
#include <platform.h>

uint64_t uptime_nanoseconds(void) {

    /* We need to protect for the timer rolling over while we're calculating total uptime so if we're too close wait for
     * the value go up */
    while (TMR0L > UPTIME_TIMER_THRESHOLD)
        ;
    uint64_t current_timer = TMR0L;

    return (uptime_milliseconds_counter * 1000000) + (current_timer * 4000);
}