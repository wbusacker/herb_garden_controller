/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  Herb Garden Controller
Filename: uptime_template.c
Purpose:  Example Function File
*/

/* CSC Includes */
#include <uptime_functions.h>

/* Library Includes */

/* Project Includes */
#include <platform.h>

// void __attribute__((interrupt, auto_psv)) _T1Interrupt(void) {
//     uptime_milliseconds_counter++;
//     _T1IF = 0;
// }

void uptime_interrupt(void) {
    uptime_milliseconds_counter++;
}