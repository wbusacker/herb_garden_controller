/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  Herb Garden Controller
Filename: uptime_data.h
Purpose:  CSC data declaration
*/

#ifndef UPTIME_DATA_H
#define UPTIME_DATA_H

/* CSC Includes */
#include <uptime_const.h>
#include <uptime_types.h>

/* Library Includes */
#include <stdint.h>

/* Project Includes */

extern volatile uint64_t uptime_milliseconds_counter;

#endif
