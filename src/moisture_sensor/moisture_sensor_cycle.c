/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  software
Filename: moisture_sensor_cycle.c
Purpose:  CSC data and initialization definitions
*/

/* CSC Includes */
#include <moisture_sensor.h>
#include <moisture_sensor_functions.h>

/* Library Includes */

/* Project Includes */

enum Common_status_code cycle_moisture_sensor(uint64_t cycle_count, void* arg) {
    enum Common_status_code return_code = COMMON_STATUS_OK;

    for (uint8_t i = 0; i < MOISTURE_SENSOR_DEVICE_COUNT; i++) {
        moisture_sensor_poll(i);
    }

    return return_code;
}
