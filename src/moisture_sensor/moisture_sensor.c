/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  software
Filename: moisture_sensor.c
Purpose:  CSC data and initialization definitions
*/

/* CSC Includes */
#include <moisture_sensor.h>
#include <moisture_sensor_functions.h>

/* Library Includes */

/* Project Includes */

enum Common_status_code init_moisture_sensor(void) {
    enum Common_status_code return_code = COMMON_STATUS_OK;

    return return_code;
}

enum Common_status_code teardown_moisture_sensor(void) {
    enum Common_status_code return_code = COMMON_STATUS_OK;

    return return_code;
}
