/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  software
Filename: moisture_sensor.h
Purpose:  External CSC Header
*/

#ifndef MOISTURE_SENSOR_H
#define MOISTURE_SENSOR_H

/* CSC Includes */
#include <moisture_sensor_const.h>
#include <moisture_sensor_types.h>

/* Library Includes */

/* Project Includes */
#include <common.h>

enum Common_status_code init_moisture_sensor(void);
enum Common_status_code teardown_moisture_sensor(void);

enum Common_status_code cycle_moisture_sensor(uint64_t cycle_count, void* arg);

#endif
