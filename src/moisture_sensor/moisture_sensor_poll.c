/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  software
Filename: moisture_sensor_template.c
Purpose:  Example Function File
*/

/* CSC Includes */
#include <moisture_sensor_functions.h>

/* Library Includes */

/* Project Includes */
#include <error_check.h>
#include <xc.h>

int32_t moisture_sensor_poll(enum Moisture_sensor_device device_id) {

    ERROR_CHECK(device_id >= MOISTURE_SENSOR_DEVICE_COUNT, "Unknown device %d\n", device_id);

    /* For performance reasons we deviate from the straight math here.

    The normal math would be

    Vref = Vdd * (2 / 5)
    Vn   = Vdd * (1 / 2)

    differential_voltage = (adc_value * Vref) / ADC_MAX_CODE;
    real_voltage         = differential_voltage + Vn;
    resistance           = (real_voltage * reference_resistor) / (Vdd - real_voltage)

    But that results in too many floating point operations being done so the math was
    reduced down to what is below

    */

    int16_t adc_value = mcp3304_get_channel(moisture_sensor_definitions[device_id].adc_channel);

    int32_t resistance = moisture_sensor_definitions[device_id].reference_resistor_ohms * (adc_value + 5120);
    resistance /= 5120 - adc_value;

    telemetry_buffer_measurand_int32_t(moisture_sensor_definitions[device_id].resistance_tlm_id, resistance);

    return resistance;
}