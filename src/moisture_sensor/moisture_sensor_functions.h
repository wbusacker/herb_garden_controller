/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  software
Filename: moisture_sensor_functions.h
Purpose:  CSC local function declarations
*/

#ifndef MOISTURE_SENSOR_FUNCTIONS_H
#define MOISTURE_SENSOR_FUNCTIONS_H

/* CSC Includes */
#include <moisture_sensor.h>
#include <moisture_sensor_data.h>

/* Library Includes */

/* Project Includes */

int32_t moisture_sensor_poll(enum Moisture_sensor_device);

#endif
