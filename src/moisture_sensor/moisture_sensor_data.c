/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  software
Filename: moisture_sensor_data.c
Purpose:  CSC data definition
*/

/* CSC Includes */
#include <moisture_sensor_data.h>

/* Library Includes */

/* Project Includes */

// clang-format off

const struct Moisture_sensor_definition moisture_sensor_definitions[MOISTURE_SENSOR_DEVICE_COUNT] = {
    [MOISTURE_SENSOR_DEVICE_0]   = {.adc_channel = MCP3304_CHANNEL_0,   .reference_resistor_ohms =  92600, .resistance_tlm_id = TELEMETRY_ID_MS_0_RAW_RESISTANCE,  .moisture_content_tlm_id = TELEMETRY_ID_MS_0_ESTIMATED_MOISTURE_CONTENT},
    [MOISTURE_SENSOR_DEVICE_1]   = {.adc_channel = MCP3304_CHANNEL_1,   .reference_resistor_ohms =  93600, .resistance_tlm_id = TELEMETRY_ID_MS_1_RAW_RESISTANCE,  .moisture_content_tlm_id = TELEMETRY_ID_MS_1_ESTIMATED_MOISTURE_CONTENT},
    [MOISTURE_SENSOR_DEVICE_2]   = {.adc_channel = MCP3304_CHANNEL_2,   .reference_resistor_ohms =  89900, .resistance_tlm_id = TELEMETRY_ID_MS_2_RAW_RESISTANCE,  .moisture_content_tlm_id = TELEMETRY_ID_MS_2_ESTIMATED_MOISTURE_CONTENT},
    [MOISTURE_SENSOR_DEVICE_3]   = {.adc_channel = MCP3304_CHANNEL_3,   .reference_resistor_ohms = 100000, .resistance_tlm_id = TELEMETRY_ID_MS_3_RAW_RESISTANCE,  .moisture_content_tlm_id = TELEMETRY_ID_MS_3_ESTIMATED_MOISTURE_CONTENT},
    [MOISTURE_SENSOR_DEVICE_4]   = {.adc_channel = MCP3304_CHANNEL_4,   .reference_resistor_ohms = 100000, .resistance_tlm_id = TELEMETRY_ID_MS_4_RAW_RESISTANCE,  .moisture_content_tlm_id = TELEMETRY_ID_MS_4_ESTIMATED_MOISTURE_CONTENT},
    [MOISTURE_SENSOR_DEVICE_5]   = {.adc_channel = MCP3304_CHANNEL_5,   .reference_resistor_ohms = 100000, .resistance_tlm_id = TELEMETRY_ID_MS_5_RAW_RESISTANCE,  .moisture_content_tlm_id = TELEMETRY_ID_MS_5_ESTIMATED_MOISTURE_CONTENT},
    [MOISTURE_SENSOR_DEVICE_6]   = {.adc_channel = MCP3304_CHANNEL_6,   .reference_resistor_ohms = 100000, .resistance_tlm_id = TELEMETRY_ID_MS_6_RAW_RESISTANCE,  .moisture_content_tlm_id = TELEMETRY_ID_MS_6_ESTIMATED_MOISTURE_CONTENT},
    [MOISTURE_SENSOR_DEVICE_7]   = {.adc_channel = MCP3304_CHANNEL_7,   .reference_resistor_ohms = 100000, .resistance_tlm_id = TELEMETRY_ID_MS_7_RAW_RESISTANCE,  .moisture_content_tlm_id = TELEMETRY_ID_MS_7_ESTIMATED_MOISTURE_CONTENT},
    [MOISTURE_SENSOR_DEVICE_8]   = {.adc_channel = MCP3304_CHANNEL_8,   .reference_resistor_ohms = 100000, .resistance_tlm_id = TELEMETRY_ID_MS_8_RAW_RESISTANCE,  .moisture_content_tlm_id = TELEMETRY_ID_MS_8_ESTIMATED_MOISTURE_CONTENT},
    [MOISTURE_SENSOR_DEVICE_9]   = {.adc_channel = MCP3304_CHANNEL_9,   .reference_resistor_ohms = 100000, .resistance_tlm_id = TELEMETRY_ID_MS_9_RAW_RESISTANCE,  .moisture_content_tlm_id = TELEMETRY_ID_MS_9_ESTIMATED_MOISTURE_CONTENT},
    [MOISTURE_SENSOR_DEVICE_10]  = {.adc_channel = MCP3304_CHANNEL_10,  .reference_resistor_ohms = 100000, .resistance_tlm_id = TELEMETRY_ID_MS_10_RAW_RESISTANCE, .moisture_content_tlm_id = TELEMETRY_ID_MS_10_ESTIMATED_MOISTURE_CONTENT},
    [MOISTURE_SENSOR_DEVICE_11]  = {.adc_channel = MCP3304_CHANNEL_11,  .reference_resistor_ohms = 100000, .resistance_tlm_id = TELEMETRY_ID_MS_11_RAW_RESISTANCE, .moisture_content_tlm_id = TELEMETRY_ID_MS_11_ESTIMATED_MOISTURE_CONTENT},
    [MOISTURE_SENSOR_DEVICE_12]  = {.adc_channel = MCP3304_CHANNEL_12,  .reference_resistor_ohms = 100000, .resistance_tlm_id = TELEMETRY_ID_MS_12_RAW_RESISTANCE, .moisture_content_tlm_id = TELEMETRY_ID_MS_12_ESTIMATED_MOISTURE_CONTENT},
    [MOISTURE_SENSOR_DEVICE_13]  = {.adc_channel = MCP3304_CHANNEL_13,  .reference_resistor_ohms = 100000, .resistance_tlm_id = TELEMETRY_ID_MS_13_RAW_RESISTANCE, .moisture_content_tlm_id = TELEMETRY_ID_MS_13_ESTIMATED_MOISTURE_CONTENT},
    [MOISTURE_SENSOR_DEVICE_14]  = {.adc_channel = MCP3304_CHANNEL_14,  .reference_resistor_ohms = 100000, .resistance_tlm_id = TELEMETRY_ID_MS_14_RAW_RESISTANCE, .moisture_content_tlm_id = TELEMETRY_ID_MS_14_ESTIMATED_MOISTURE_CONTENT},
    [MOISTURE_SENSOR_DEVICE_15]  = {.adc_channel = MCP3304_CHANNEL_15,  .reference_resistor_ohms = 100000, .resistance_tlm_id = TELEMETRY_ID_MS_15_RAW_RESISTANCE, .moisture_content_tlm_id = TELEMETRY_ID_MS_15_ESTIMATED_MOISTURE_CONTENT},
    [MOISTURE_SENSOR_DEVICE_16]  = {.adc_channel = MCP3304_CHANNEL_16,  .reference_resistor_ohms = 100000, .resistance_tlm_id = TELEMETRY_ID_MS_16_RAW_RESISTANCE, .moisture_content_tlm_id = TELEMETRY_ID_MS_16_ESTIMATED_MOISTURE_CONTENT},
    [MOISTURE_SENSOR_DEVICE_17]  = {.adc_channel = MCP3304_CHANNEL_17,  .reference_resistor_ohms = 100000, .resistance_tlm_id = TELEMETRY_ID_MS_17_RAW_RESISTANCE, .moisture_content_tlm_id = TELEMETRY_ID_MS_17_ESTIMATED_MOISTURE_CONTENT},
    [MOISTURE_SENSOR_DEVICE_18]  = {.adc_channel = MCP3304_CHANNEL_18,  .reference_resistor_ohms = 100000, .resistance_tlm_id = TELEMETRY_ID_MS_18_RAW_RESISTANCE, .moisture_content_tlm_id = TELEMETRY_ID_MS_18_ESTIMATED_MOISTURE_CONTENT},
    [MOISTURE_SENSOR_DEVICE_19]  = {.adc_channel = MCP3304_CHANNEL_19,  .reference_resistor_ohms = 100000, .resistance_tlm_id = TELEMETRY_ID_MS_19_RAW_RESISTANCE, .moisture_content_tlm_id = TELEMETRY_ID_MS_19_ESTIMATED_MOISTURE_CONTENT},
    [MOISTURE_SENSOR_DEVICE_20]  = {.adc_channel = MCP3304_CHANNEL_20,  .reference_resistor_ohms = 100000, .resistance_tlm_id = TELEMETRY_ID_MS_20_RAW_RESISTANCE, .moisture_content_tlm_id = TELEMETRY_ID_MS_20_ESTIMATED_MOISTURE_CONTENT},
    [MOISTURE_SENSOR_DEVICE_21]  = {.adc_channel = MCP3304_CHANNEL_21,  .reference_resistor_ohms = 100000, .resistance_tlm_id = TELEMETRY_ID_MS_21_RAW_RESISTANCE, .moisture_content_tlm_id = TELEMETRY_ID_MS_21_ESTIMATED_MOISTURE_CONTENT},
    [MOISTURE_SENSOR_DEVICE_22]  = {.adc_channel = MCP3304_CHANNEL_22,  .reference_resistor_ohms = 100000, .resistance_tlm_id = TELEMETRY_ID_MS_22_RAW_RESISTANCE, .moisture_content_tlm_id = TELEMETRY_ID_MS_22_ESTIMATED_MOISTURE_CONTENT},
    [MOISTURE_SENSOR_DEVICE_23]  = {.adc_channel = MCP3304_CHANNEL_23,  .reference_resistor_ohms = 100000, .resistance_tlm_id = TELEMETRY_ID_MS_23_RAW_RESISTANCE, .moisture_content_tlm_id = TELEMETRY_ID_MS_23_ESTIMATED_MOISTURE_CONTENT},
    [MOISTURE_SENSOR_DEVICE_24]  = {.adc_channel = MCP3304_CHANNEL_24,  .reference_resistor_ohms = 100000, .resistance_tlm_id = TELEMETRY_ID_MS_24_RAW_RESISTANCE, .moisture_content_tlm_id = TELEMETRY_ID_MS_24_ESTIMATED_MOISTURE_CONTENT},
    [MOISTURE_SENSOR_DEVICE_25]  = {.adc_channel = MCP3304_CHANNEL_25,  .reference_resistor_ohms = 100000, .resistance_tlm_id = TELEMETRY_ID_MS_25_RAW_RESISTANCE, .moisture_content_tlm_id = TELEMETRY_ID_MS_25_ESTIMATED_MOISTURE_CONTENT},
    [MOISTURE_SENSOR_DEVICE_26]  = {.adc_channel = MCP3304_CHANNEL_26,  .reference_resistor_ohms = 100000, .resistance_tlm_id = TELEMETRY_ID_MS_26_RAW_RESISTANCE, .moisture_content_tlm_id = TELEMETRY_ID_MS_26_ESTIMATED_MOISTURE_CONTENT},
    [MOISTURE_SENSOR_DEVICE_27]  = {.adc_channel = MCP3304_CHANNEL_27,  .reference_resistor_ohms = 100000, .resistance_tlm_id = TELEMETRY_ID_MS_27_RAW_RESISTANCE, .moisture_content_tlm_id = TELEMETRY_ID_MS_27_ESTIMATED_MOISTURE_CONTENT},
    [MOISTURE_SENSOR_DEVICE_28]  = {.adc_channel = MCP3304_CHANNEL_28,  .reference_resistor_ohms = 100000, .resistance_tlm_id = TELEMETRY_ID_MS_28_RAW_RESISTANCE, .moisture_content_tlm_id = TELEMETRY_ID_MS_28_ESTIMATED_MOISTURE_CONTENT},
    [MOISTURE_SENSOR_DEVICE_29]  = {.adc_channel = MCP3304_CHANNEL_29,  .reference_resistor_ohms = 100000, .resistance_tlm_id = TELEMETRY_ID_MS_29_RAW_RESISTANCE, .moisture_content_tlm_id = TELEMETRY_ID_MS_29_ESTIMATED_MOISTURE_CONTENT},
    [MOISTURE_SENSOR_DEVICE_30]  = {.adc_channel = MCP3304_CHANNEL_30,  .reference_resistor_ohms = 100000, .resistance_tlm_id = TELEMETRY_ID_MS_30_RAW_RESISTANCE, .moisture_content_tlm_id = TELEMETRY_ID_MS_30_ESTIMATED_MOISTURE_CONTENT},
    [MOISTURE_SENSOR_DEVICE_31]  = {.adc_channel = MCP3304_CHANNEL_31,  .reference_resistor_ohms = 100000, .resistance_tlm_id = TELEMETRY_ID_MS_31_RAW_RESISTANCE, .moisture_content_tlm_id = TELEMETRY_ID_MS_31_ESTIMATED_MOISTURE_CONTENT},
    [MOISTURE_SENSOR_DEVICE_32]  = {.adc_channel = MCP3304_CHANNEL_32,  .reference_resistor_ohms = 100000, .resistance_tlm_id = TELEMETRY_ID_MS_32_RAW_RESISTANCE, .moisture_content_tlm_id = TELEMETRY_ID_MS_32_ESTIMATED_MOISTURE_CONTENT},
    [MOISTURE_SENSOR_DEVICE_33]  = {.adc_channel = MCP3304_CHANNEL_33,  .reference_resistor_ohms = 100000, .resistance_tlm_id = TELEMETRY_ID_MS_33_RAW_RESISTANCE, .moisture_content_tlm_id = TELEMETRY_ID_MS_33_ESTIMATED_MOISTURE_CONTENT},
    [MOISTURE_SENSOR_DEVICE_34]  = {.adc_channel = MCP3304_CHANNEL_34,  .reference_resistor_ohms = 100000, .resistance_tlm_id = TELEMETRY_ID_MS_34_RAW_RESISTANCE, .moisture_content_tlm_id = TELEMETRY_ID_MS_34_ESTIMATED_MOISTURE_CONTENT},
    [MOISTURE_SENSOR_DEVICE_35]  = {.adc_channel = MCP3304_CHANNEL_35,  .reference_resistor_ohms = 100000, .resistance_tlm_id = TELEMETRY_ID_MS_35_RAW_RESISTANCE, .moisture_content_tlm_id = TELEMETRY_ID_MS_35_ESTIMATED_MOISTURE_CONTENT}
};

// clang-format on