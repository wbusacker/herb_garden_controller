/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  software
Filename: moisture_sensor_data.h
Purpose:  CSC data declaration
*/

#ifndef MOISTURE_SENSOR_DATA_H
#define MOISTURE_SENSOR_DATA_H

/* CSC Includes */
#include <moisture_sensor_const.h>
#include <moisture_sensor_types.h>

/* Library Includes */

/* Project Includes */

extern const struct Moisture_sensor_definition moisture_sensor_definitions[MOISTURE_SENSOR_DEVICE_COUNT];

#endif
