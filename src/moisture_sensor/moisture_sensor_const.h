/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  software
Filename: moisture_sensor_const.h
Purpose:  CSC constants
*/

#ifndef MOISTURE_SENSOR_CONST_H
#define MOISTURE_SENSOR_CONST_H

/* CSC Includes */
#include <moisture_sensor_allocation.h>

/* Library Includes */

/* Project Includes */
#include <common.h>

#define MOISTURE_SENSOR_VREF (COMMON_RAIL_5V * (2.0 / 5.0))
#define MOISTURE_SENSOR_V_N  (COMMON_RAIL_5V * 0.5)

#endif
