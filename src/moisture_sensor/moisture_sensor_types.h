/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  software
Filename: moisture_sensor_types.h
Purpose:  CSC data types
*/

#ifndef MOISTURE_SENSOR_TYPES_H
#define MOISTURE_SENSOR_TYPES_H

/* CSC Includes */
#include <moisture_sensor_const.h>

/* Library Includes */

/* Project Includes */
#include <mcp3304.h>
#include <telemetry.h>

struct Moisture_sensor_definition {
    enum MCP3304_channel         adc_channel;
    int32_t                      reference_resistor_ohms;
    enum Telemetry_measurand_ids resistance_tlm_id;
    enum Telemetry_measurand_ids moisture_content_tlm_id;
};

#endif
