/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  Herb Garden Controller
Filename: status_data.c
Purpose:  CSC data definition
*/

/* CSC Includes */
#include <status_data.h>

/* Library Includes */

/* Project Includes */
