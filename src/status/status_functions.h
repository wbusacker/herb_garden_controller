/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  Herb Garden Controller
Filename: status_functions.h
Purpose:  CSC local function declarations
*/

#ifndef STATUS_FUNCTIONS_H
#define STATUS_FUNCTIONS_H

/* CSC Includes */
#include <status.h>
#include <status_data.h>

/* Library Includes */

/* Project Includes */

#endif
