/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  Herb Garden Controller
Filename: status.c
Purpose:  CSC data and initialization definitions
*/

/* CSC Includes */
#include <status.h>
#include <status_functions.h>

/* Library Includes */

/* Project Includes */
#include <io.h>

enum Common_status_code init_status(void) {
    enum Common_status_code return_code = COMMON_STATUS_OK;

    io_set_pin_function(IO_PIN_STATUS_LED, IO_PIN_FUNCTION_DISCRETE_OUT_ACTIVE_LOW);

    io_set_pin_function(IO_PIN_EXPANDER_0_1, IO_PIN_FUNCTION_DISCRETE_OUT_ACTIVE_LOW);
    io_set_pin_function(IO_PIN_EXPANDER_0_2, IO_PIN_FUNCTION_DISCRETE_OUT_ACTIVE_LOW);
    io_set_pin_function(IO_PIN_EXPANDER_0_3, IO_PIN_FUNCTION_DISCRETE_OUT_ACTIVE_LOW);
    io_set_pin_function(IO_PIN_EXPANDER_0_4, IO_PIN_FUNCTION_DISCRETE_OUT_ACTIVE_LOW);
    io_set_pin_function(IO_PIN_EXPANDER_0_5, IO_PIN_FUNCTION_DISCRETE_OUT_ACTIVE_LOW);
    io_set_pin_function(IO_PIN_EXPANDER_0_6, IO_PIN_FUNCTION_DISCRETE_OUT_ACTIVE_LOW);
    io_set_pin_function(IO_PIN_EXPANDER_0_7, IO_PIN_FUNCTION_DISCRETE_OUT_ACTIVE_LOW);
    io_set_pin_function(IO_PIN_EXPANDER_0_8, IO_PIN_FUNCTION_DISCRETE_OUT_ACTIVE_LOW);
    io_set_pin_function(IO_PIN_EXPANDER_0_21, IO_PIN_FUNCTION_DISCRETE_OUT_ACTIVE_LOW);
    io_set_pin_function(IO_PIN_EXPANDER_0_22, IO_PIN_FUNCTION_DISCRETE_OUT_ACTIVE_LOW);
    io_set_pin_function(IO_PIN_EXPANDER_0_23, IO_PIN_FUNCTION_DISCRETE_OUT_ACTIVE_LOW);
    io_set_pin_function(IO_PIN_EXPANDER_0_24, IO_PIN_FUNCTION_DISCRETE_OUT_ACTIVE_LOW);
    io_set_pin_function(IO_PIN_EXPANDER_0_25, IO_PIN_FUNCTION_DISCRETE_OUT_ACTIVE_LOW);
    io_set_pin_function(IO_PIN_EXPANDER_0_26, IO_PIN_FUNCTION_DISCRETE_OUT_ACTIVE_LOW);
    io_set_pin_function(IO_PIN_EXPANDER_0_27, IO_PIN_FUNCTION_DISCRETE_OUT_ACTIVE_LOW);
    io_set_pin_function(IO_PIN_EXPANDER_0_28, IO_PIN_FUNCTION_DISCRETE_OUT_ACTIVE_LOW);

    return return_code;
}

enum Common_status_code teardown_status(void) {
    enum Common_status_code return_code = COMMON_STATUS_OK;

    return return_code;
}
