/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  Herb Garden Controller
Filename: status_cycle.c
Purpose:  CSC data and initialization definitions
*/

/* CSC Includes */
#include <status.h>
#include <status_functions.h>

/* Library Includes */

/* Project Includes */
#include <io.h>
#include <uptime.h>

enum IO_pin pin_walk_list[16] = {IO_PIN_EXPANDER_0_1,
                                 IO_PIN_EXPANDER_0_2,
                                 IO_PIN_EXPANDER_0_3,
                                 IO_PIN_EXPANDER_0_4,
                                 IO_PIN_EXPANDER_0_5,
                                 IO_PIN_EXPANDER_0_6,
                                 IO_PIN_EXPANDER_0_7,
                                 IO_PIN_EXPANDER_0_8,
                                 IO_PIN_EXPANDER_0_21,
                                 IO_PIN_EXPANDER_0_22,
                                 IO_PIN_EXPANDER_0_23,
                                 IO_PIN_EXPANDER_0_24,
                                 IO_PIN_EXPANDER_0_25,
                                 IO_PIN_EXPANDER_0_26,
                                 IO_PIN_EXPANDER_0_27,
                                 IO_PIN_EXPANDER_0_28};

enum Common_status_code cycle_status(uint64_t cycle_count, void* arg) {
    enum Common_status_code return_code = COMMON_STATUS_OK;

    enum IO_state state = io_read(IO_PIN_STATUS_LED);
    state               = state == IO_SET ? IO_CLEAR : IO_SET;

    io_set(IO_PIN_STATUS_LED, state);

    static uint8_t pin_num = 0;

    for (uint8_t i = 0; i < 16; i++) {
        if (i == pin_num) {
            io_set(pin_walk_list[i], IO_SET);

        } else {
            io_set(pin_walk_list[i], IO_CLEAR);
        }
    }

    pin_num++;
    pin_num %= 16;

    return return_code;
}
