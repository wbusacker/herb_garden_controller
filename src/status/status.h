/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  Herb Garden Controller
Filename: status.h
Purpose:  External CSC Header
*/

#ifndef STATUS_H
#define STATUS_H

/* CSC Includes */
#include <status_const.h>
#include <status_types.h>

/* Library Includes */

/* Project Includes */
#include <common.h>

enum Common_status_code init_status(void);
enum Common_status_code teardown_status(void);

enum Common_status_code cycle_status(uint64_t cycle_count, void* arg);

#endif
