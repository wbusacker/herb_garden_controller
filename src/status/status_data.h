/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  Herb Garden Controller
Filename: status_data.h
Purpose:  CSC data declaration
*/

#ifndef STATUS_DATA_H
#define STATUS_DATA_H

/* CSC Includes */
#include <status_const.h>
#include <status_types.h>

/* Library Includes */

/* Project Includes */

#endif
