/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  Herb Garden Controller
Filename: status_types.h
Purpose:  CSC data types
*/

#ifndef STATUS_TYPES_H
#define STATUS_TYPES_H

/* CSC Includes */
#include <status_const.h>

/* Library Includes */

/* Project Includes */

#endif
