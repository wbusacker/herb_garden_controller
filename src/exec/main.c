/*
Copyright (c) 2024 Will Busacker
See project license for more details

Project:  Herb Garden Controller
Filename: main.c
Purpose:  Program entry point
*/

/* CSC Includes */

/* Library Includes */
#include <stddef.h>

/* Project Includes */
#include <common.h>
#include <io.h>
#include <log.h>
#include <mcp23s17.h>
#include <mcp3304.h>
#include <moisture_sensor.h>
#include <pic18_io.h>
#include <platform.h>
#include <rate_monotonic.h>
#include <spi.h>
#include <status.h>
#include <tasking.h>
#include <telemetry.h>
#include <uart.h>
#include <uptime.h>

struct CSC_interface {
    enum Common_status_code (*init)(void);
    enum Common_status_code (*teardown)(void);
    bool initialized;
};

// clang-format off

/* CSC initialization order is ordered such that the first listed CSCs are initialized first

Groups are formed such that any CSCs grouped together don't interdepend on eachother but 
they depend on something in the next highest group

*/
struct CSC_interface CSCS[] = {

    /* No Dependancies */
    {.init = init_uptime,            .teardown = teardown_uptime           }, /* Need to start the timers asap*/

    {.init = init_telemetry,         .teardown = teardown_telemetry        },
    {.init = init_common,            .teardown = teardown_common           },
    {.init = init_pic18_io,          .teardown = teardown_pic18_io         },
    {.init = init_tasking,           .teardown = teardown_tasking          },

    /* Init Group 2 */
    {.init = init_io,                .teardown = teardown_io               },

    /* Init Group 3 */
    {.init = init_uart,              .teardown = teardown_uart             },

    /* Init Group 4 */
    {.init = init_log,               .teardown = teardown_log              },

    /* Init Group 5 */
    {.init = init_spi,               .teardown = teardown_spi              },
    {.init = init_rate_monotonic,    .teardown = teardown_rate_monotonic   },
    {.init = init_tasking,           .teardown = teardown_tasking          },

    /* Init Group 6 */
    {.init = init_mcp23s17,          .teardown = teardown_mcp23s17         },
    {.init = init_mcp3304,           .teardown = teardown_mcp3304          },

    /* Init Group 7 */
    {.init = init_moisture_sensor,   .teardown = teardown_moisture_sensor  },

    /* Init Group 8 */
    {.init = init_status,            .teardown = teardown_status           },

};
uint16_t CSC_COUNT = sizeof(CSCS) / sizeof(CSCS[0]);

struct Tasking_entry TASKS[] = {
    {.function = cycle_status,          .arguments = NULL, .frequency = TASKING_FREQUENCY_1_HZ , .min_runtime_tlm_id = TELEMETRY_ID_TASK_0_MIN_RUNTIME_NS, .max_runtime_tlm_id = TELEMETRY_ID_TASK_0_MAX_RUNTIME_NS, .ave_runtime_tlm_id = TELEMETRY_ID_TASK_0_AVE_RUNTIME_NS},
    {.function = cycle_moisture_sensor, .arguments = NULL, .frequency = TASKING_FREQUENCY_10_HZ, .min_runtime_tlm_id = TELEMETRY_ID_TASK_1_MIN_RUNTIME_NS, .max_runtime_tlm_id = TELEMETRY_ID_TASK_1_MAX_RUNTIME_NS, .ave_runtime_tlm_id = TELEMETRY_ID_TASK_1_AVE_RUNTIME_NS},
    {.function = cycle_telemetry,       .arguments = NULL, .frequency = TASKING_FREQUENCY_10_HZ, .min_runtime_tlm_id = TELEMETRY_ID_TASK_2_MIN_RUNTIME_NS, .max_runtime_tlm_id = TELEMETRY_ID_TASK_2_MAX_RUNTIME_NS, .ave_runtime_tlm_id = TELEMETRY_ID_TASK_2_AVE_RUNTIME_NS},
};
uint16_t TASK_COUNT = sizeof(TASKS) / sizeof(TASKS[0]);

// clang-format on

int main(void) {

    platform_init();

    /* First mark all CSCs as uninitialized */
    for (uint16_t csc = 0; csc < CSC_COUNT; csc++) {
        CSCS[csc].initialized = false;
    }

    /* Attempt initialization of all CSCs */
    bool all_init = true;
    for (uint16_t csc = 0; csc < CSC_COUNT; csc++) {
        CSCS[csc].initialized          = true;
        enum Common_status_code status = CSCS[csc].init();

        if (status != COMMON_STATUS_OK) {
            all_init = false;
            break;
        }
    }

    if (all_init) {
        LOG_ALWAYS("CSC Initialization Success\n");

        for (uint16_t csc = 0; csc < CSC_COUNT; csc++) {
            LOG_ALWAYS("CSC %d started\n", csc);
        }

        enum Common_status_code status = tasking_cycle(TASKS, TASK_COUNT);

        LOG_ALWAYS("Tasking returned with code %s\n", common_get_status_code_string(status));

    } else {
        LOG_ALWAYS("Not all CSCs initialized\n");
    }

    /* Teardown all CSCS in reverse order */
    for (int16_t csc = (int16_t)CSC_COUNT; csc >= 0; csc--) {

        if (CSCS[csc].initialized) {
            enum Common_status_code status = CSCS[csc].teardown();

            if (status != COMMON_STATUS_OK) {
                all_init = false;
                break;
            }
        }
    }

    return (0);
}