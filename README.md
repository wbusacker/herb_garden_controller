# Base C Project

This is a starting point for GCC based C/C++ programs with a suite of build options

## Expected Code Structure

The build system assumes a semi-flat source code organization. Code is expected to be organized into logical units following DOD-STD-2167A sections 4.2.5 and 4.2.9, namely utilizing the concept of a Computer Software Component (CSC) as a basis of encapsulation and categorization of logic. The source and test directories are expected to be filled with CSC directories, the contents of which are a flat source file layout with both header files and source files intermixed. 

## Header Inclusion

Due to the expected code organization, all CSC folders are added into the compilation command allowing for the `#include <>` format to be used for including header files

# CSC Organization

## Externally Visible Files

### csc_allocation.h

If a CSC contains a list of devices/objects that other CSCs use, an Enumeration with generic names is listed here for all instances of those devices/objects with preprocessor defines providing unique names

### csc_const.h

Any non-mutable symbols for the CSC.

Includes language features like

- Preprocessor `#defines`
- Enumerations

### csc_types.h

Data structures/type overrides for the CSC

Includes language features 

- Structs
- Unions
- `typedef`

### csc.h

File that is included to be used by any external CSC

Includes language features:

- Function prototypes

## Intenally Visibile Files

### csc_functions.h

File that is included for all internal csc function files

Includes language features:

- Function prototypes

### csc_data.[c|h]


# CSC Architecture

Each CSC is largely responsible for all logic and configuration management for its respective roll/devices. For example, and ADC CSC shall be aware of all physical instances of the ADCs in the system, even if the CSC itself does not know what they are used for. 

# Configuration Tables

If there is data that can be expressed in an array of structs or base types, they should be stored in a configuration table. A configuration table format should include an enumeration named `enum csc_*item*`, with the last item being `CSC_*ITEM*_COUNT`, the data structure being named `struct csc_*item*_config`, and instance of the built data table named `const struct csc_*item*_config csc_*item*_configs[CSC_*ITEM*_COUNT];`

If the configuration table can have information defined at compile time, the array should be marked as `const` and the word `config` swapped out for `definition`