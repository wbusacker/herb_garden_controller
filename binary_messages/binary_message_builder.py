#!/usr/bin/python3

import json
import math
import os

AUTOCODE_DIRECTORY           = "src/autocode/"
MESSAGE_DEFINITION_DIRECTORY = "binary_messages/devices/"

class Byte_field:
    def __init__(self, byte_index, bit_index, bit_length, shift_offset):
        self.byte_index = byte_index
        self.bit_index  = bit_index
        self.bit_length = bit_length
        self.shift_offset = shift_offset

class Field:

    def __init__(self, json_data, bit_offset, struct_name):
        self.name         = json_data["name"]
        self.bit_length   = json_data["bit_length"]
        self.data_type    = json_data["c_data_type"]
        self.struct_name  = struct_name
        self.static       = False
        self.static_value = None
        self.bit_offset   = bit_offset
        self.bytes_span   = []

        working_bit_offset = bit_offset
        bits_remaining     = self.bit_length
        while bits_remaining > 0:

            byte_index = int(working_bit_offset / 8)
            bit_index  = 7 - (working_bit_offset % 8)
            bit_length = bit_index + 1

            if(bit_length > bits_remaining):
                bit_length = bits_remaining
                
            bits_remaining -= bit_length

            self.bytes_span.append(Byte_field(byte_index, bit_index, bit_length, bits_remaining))

            working_bit_offset += bit_length

        if "static_value" in json_data:
            self.static       = True
            self.static_value = int(json_data["static_value"])

    def get_c_struct_entry(self):

        return "{:7s} {:s}".format(self.data_type, self.name)
    
    def get_c_pack(self):

        lines = ""

        accessor = "data->{:s}".format(self.name)

        if self.static:
            lines += "{:s} = {};\n".format(accessor, self.static_value)

        if self.data_type == "float" : 
            lines += "memcpy(&working_bit_buffer, {:s}, 4);\n".format(accessor)
            accessor = "working_bit_buffer"

        if self.data_type == "double" : 
            lines += "memcpy(&working_bit_buffer, {:s}, 8);\n".format(accessor)
            accessor = "working_bit_buffer"

        for format in self.bytes_span:

            entry = accessor

            if format.shift_offset != 0:
                entry = "({:s} >> {:d})".format(entry, format.shift_offset)

            if format.bit_length != 8:
                mask = int(2 ** format.bit_length) - 1
                entry = "({:s} & 0x{:x})".format(entry, mask)

            potential_shift = (format.bit_index - format.bit_length) + 1
                
            if potential_shift != 0:
                entry = "({:s} << {:d})".format(entry, potential_shift)

            lines += "buffer[{:d}] |= (uint8_t){:s};\n".format(format.byte_index, entry)

        return lines
    
    def get_c_unpack(self):

        lines = ""

        accessor = "data->{:s}".format(self.name)

        larger_data_type = self.data_type

        if self.data_type == "float" : 
            accessor = "working_bit_buffer"
            larger_data_type = "uint32_t"

        if self.data_type == "double" : 
            accessor = "working_bit_buffer"
            larger_data_type = "uint64_t"

        for format in self.bytes_span:

            entry = "({:s})buffer[{:d}]".format(larger_data_type, format.byte_index)

            potential_shift = (format.bit_index - format.bit_length) + 1
            if potential_shift != 0:
                entry = "({:s} >> {:d})".format(entry, potential_shift)

            if format.bit_length != 8:
                mask = int(2 ** format.bit_length) - 1
                entry = "({:s} & 0x{:x})".format(entry, mask)

            if format.shift_offset != 0:
                entry = "({:s} << {:d})".format(entry, format.shift_offset)

            lines += "{:s} |= {:s};\n".format(accessor, entry)

        if self.data_type == "float" : 
            lines += "memcpy({:s}, &working_bit_buffer, 4);\n".format(accessor)

        if self.data_type == "double" : 
            lines += "memcpy({:s}, &working_bit_buffer, 8);\n".format(accessor)

        signed_bits = {
            "int8_t"  : 0xFF,
            "int16_t" : 0xFFFF,
            "int32_t" : 0xFFFFFFFF,
            "int64_t" : 0xFFFFFFFFFFFF
        }

        if self.data_type in signed_bits:
            sign_bit = 1 << (self.bit_length - 1)
            sign_mask = signed_bits[self.data_type] ^ (sign_bit - 1)

            lines += "if (({:s} & 0x{:x}) != 0){{\n".format(accessor, sign_bit)
            lines +=     "{:s} |= 0x{:x};\n".format(accessor, sign_mask)
            lines += "}"

        return lines

class Message:
    def __init__(self, device_name, message_data):
        self.name = message_data["name"].replace(" ", "_")
        self.struct_name = "Message_{:s}_{:s}".format(device_name.lower(), self.name.lower())
        self.fields = []
        
        working_bit = 0

        for field in message_data["fields"]:
            self.fields.append(Field(field, working_bit, self.struct_name))
            working_bit += field["bit_length"]

        self.buffer_len_bytes = math.ceil(working_bit / 8)
        self.buffer_len_define = "MESSAGE_{:s}_{:s}_BUFFER_LEN".format(device_name.upper(), self.name.upper())

        function_signature_format = "void {struct_name_lower:s}_{{mode:s}}(struct {struct_name:s}* data, uint8_t buffer[{buffer_len:s}])".format(struct_name_lower = self.struct_name.lower(),
                                                                                                                                                 struct_name = self.struct_name,
                                                                                                                                        buffer_len = self.buffer_len_define)

        self.pack_function_signature   = function_signature_format.format(mode = "pack")
        self.unpack_function_signature = function_signature_format.format(mode = "unpack")


    def get_c_header_data(self):

        lines = "#define {:s} ({:d})\n".format(self.buffer_len_define, self.buffer_len_bytes)
        lines += "\n"

        lines += "struct {:s} {{\n".format(self.struct_name)

        for field in self.fields:
            lines += "    {:s};\n".format(field.get_c_struct_entry())

        lines += "};\n"
        lines += "\n"

        lines += "{:s};\n".format(self.pack_function_signature)
        lines += "{:s};\n".format(self.unpack_function_signature)
        lines += "\n"

        return lines

    def get_c_source_data(self):

        lines = "{:s}{{\n".format(self.pack_function_signature)

        lines += "    for(uint8_t i = 0; i < {:s}; i++){{\n".format(self.buffer_len_define)
        lines += "        buffer[i] = 0;\n"
        lines += "    }\n"
        lines += "\n"

        lines += "    uint64_t working_bit_buffer = 0;\n"
        lines += "\n"

        for field in self.fields:
            lines += "    {:s}".format(field.get_c_pack())

        lines += "\n"
        lines += "}\n"
        lines += "\n"

        lines += "{:s}{{\n".format(self.unpack_function_signature)

        lines += "    uint64_t working_bit_buffer = 0;\n"
        lines += "\n"

        for field in self.fields:
            lines += "    {:s}".format(field.get_c_unpack())

        lines += "\n"
        lines += "}\n"

        return lines

def build_binary_message(definition_file):

    message_list = json.load(open(definition_file))
    messages_name = definition_file.split("/")[-1].split(".")[0]

    messages = []
    for message in message_list:
        messages.append(Message(messages_name, message))


    with open("{:s}/message_{:s}_autocode.h".format(AUTOCODE_DIRECTORY, messages_name), "w") as fh:

        header_macro = "MESSAGES_{:s}_H".format(messages_name.upper())

        for message in messages:
            fh.write("/* MESSAGES AUTOCODE */\n")
            fh.write("\n")
            fh.write("#ifndef {:s}\n".format(header_macro))
            fh.write("#define {:s}\n".format(header_macro))
            fh.write("\n")
            fh.write("#include <stdint.h>\n")
            fh.write("\n")

            fh.write(message.get_c_header_data())

            fh.write("#endif")

    with open("{:s}/message_{:s}_autocode.c".format(AUTOCODE_DIRECTORY, messages_name), "w") as fh:

        for message in messages:
            fh.write("/* MESSAGES AUTOCODE */\n")
            fh.write("\n")
            fh.write("#include <message_{:s}_autocode.h>\n".format(messages_name))
            fh.write("\n")

            fh.write(message.get_c_source_data())



for file in os.listdir(path=MESSAGE_DEFINITION_DIRECTORY):
    path = MESSAGE_DEFINITION_DIRECTORY + "/" + file
    build_binary_message(path)