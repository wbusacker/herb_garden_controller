#!/usr/bin/python3

import json
import os

def write_autogen_header_file(filename, filebody):

    with open("src/autocode/" + filename + ".h", "w") as fh:

        fh.write("/*\n")
        fh.write("TELEMETRY GEN AUTOCODE\n")
        fh.write("*/\n\n")

        fh.write("#ifndef {:s}_H\n".format(filename.upper()))
        fh.write("#define {:s}_H\n\n".format(filename.upper()))

        fh.write(filebody)

        fh.write("\n\n#endif")

def write_autogen_source_file(filename, filebody):

    with open("src/autocode/" + filename + ".c", "w") as fh:

        fh.write("/*\n")
        fh.write("TELEMETRY GEN AUTOCODE\n")
        fh.write("*/\n\n")

        fh.write(filebody)

class Telemetry_item:

    DATA_TYPE_SIZES = {
        "uint8_t"  : 1,
        "uint16_t" : 2,
        "uint32_t" : 4,
        "uint64_t" : 8,
        "int8_t"   : 1,
        "int16_t"  : 2,
        "int32_t"  : 4,
        "int64_t"  : 8,
        "float"    : 4,
        "double"   : 8
    }

    def __init__(self, name, data_type):
        self.fully_qualified_name = name
        self.data_type            = data_type
        self.id                   = None
        self.buffer_start_byte    = None

        self.name_upper_snake = self.fully_qualified_name.upper().replace(" ", "_").replace("-","_")

        self.c_macro_id = "TELEMETRY_ID_{:s}".format(self.name_upper_snake)

    def assign_id(self, id, buffer_counter):
        self.id                = id
        self.buffer_start_byte = buffer_counter

    def __str__(self):
        return "{:s} {:s} {:}".format(self.fully_qualified_name, self.data_type, self.id)

def build_telemetry_points(tlm_map, point_list, parent_alias):

    prefix = ""
    if parent_alias != "":
        prefix = parent_alias + " "

    for point in point_list:
        tlm_map.append(Telemetry_item(prefix + point["name"], point["data_type"]))

def build_sub_device_points(tlm_map, device_list, parent_alias, master_device_list):

    for device in device_list:

        prefix = ""
        if parent_alias != "":
            prefix = parent_alias + " "

        if "alias" in device:
            prefix += device["alias"]
        else:
            prefix += device["device"]

        build_telemetry_points(tlm_map, master_device_list[device["device"]]["telemetry_points"], prefix)

        build_sub_device_points(tlm_map, master_device_list[device["device"]]["sub_devices"], prefix, master_device_list)

def generate_tlm_id_header(tlm_map, buffer_counter):
    
    filebody = "#include <stdint.h>\n\n"

    # Find the longest name in the C IDS
    max_c_macro_id_len   = 0
    max_id         = 0

    for item in tlm_map:
        if len(item.c_macro_id) > max_c_macro_id_len:
            max_c_macro_id_len = len(item.c_macro_id)
        if item.id > max_id:
            max_id = item.id

    filebody += "#define TELEMETRY_AUTOCODE_MAX_BUFFER_LEN ({:d})\n\n".format(buffer_counter)

    filebody += "enum Telemetry_measurand_ids {\n"

    entry_format = "    {{:{:d}s}} = {{:d}},\n".format(max_c_macro_id_len)

    for item in tlm_map:
        filebody += entry_format.format(item.c_macro_id, item.id)
        
    filebody += entry_format.format("TELEMETRY_ID_COUNT", max_id + 1)

    filebody += "};\n"

    filebody += "\nextern const uint16_t telemetry_autocode_buffer_locations[TELEMETRY_ID_COUNT];\n"
    filebody += "\n"

    filebody += "void telemetry_buffer_measurand_uint8_t(enum Telemetry_measurand_ids telemetry_id, uint8_t data);\n"
    filebody += "void telemetry_buffer_measurand_uint16_t(enum Telemetry_measurand_ids telemetry_id, uint16_t data);\n"
    filebody += "void telemetry_buffer_measurand_uint32_t(enum Telemetry_measurand_ids telemetry_id, uint32_t data);\n"
    filebody += "void telemetry_buffer_measurand_uint64_t(enum Telemetry_measurand_ids telemetry_id, uint64_t data);\n"
    filebody += "\n"
    filebody += "void telemetry_buffer_measurand_int8_t(enum Telemetry_measurand_ids telemetry_id, int8_t data);\n"
    filebody += "void telemetry_buffer_measurand_int16_t(enum Telemetry_measurand_ids telemetry_id, int16_t data);\n"
    filebody += "void telemetry_buffer_measurand_int32_t(enum Telemetry_measurand_ids telemetry_id, int32_t data);\n"
    filebody += "void telemetry_buffer_measurand_int64_t(enum Telemetry_measurand_ids telemetry_id, int64_t data);\n"
    filebody += "\n"
    filebody += "void telemetry_buffer_measurand_float(enum Telemetry_measurand_ids telemetry_id, float data);\n"
    filebody += "void telemetry_buffer_measurand_double(enum Telemetry_measurand_ids telemetry_id, double data);\n"
    filebody += "\n"

    for item in tlm_map:
        filebody += "#define TELEMETRY_{id:s}_BUFFER(x) telemetry_buffer_measurand_{data_type:s}({macro_id:s}, x)\n".format(id = item.name_upper_snake, data_type = item.data_type, macro_id = item.c_macro_id)


    write_autogen_header_file("telemetry_autocode_tlm_ids", filebody)

def generate_tlm_map_file(tlm_map):

    filebody = "#include <telemetry_autocode_tlm_ids.h>\n\n"
    
    max_c_macro_id_len = 0

    for item in tlm_map:
        if len(item.c_macro_id) > max_c_macro_id_len:
            max_c_macro_id_len = len(item.c_macro_id)

    entry_format = "    [{{:{:d}s}}] = {{:d}},\n".format(max_c_macro_id_len)

    filebody += "const uint16_t telemetry_autocode_buffer_locations[TELEMETRY_ID_COUNT] = {\n"

    for item in tlm_map:
        filebody += entry_format.format(item.c_macro_id, item.buffer_start_byte)

    filebody += "};\n"

    write_autogen_source_file("telemetry_autocode_tlm_ids", filebody)


def generate_tlm_decoder_map(tlm_map):

    with open("tlm_map.csv", "w") as fh:

        for item in tlm_map:
            fh.write("{display_name:s},{data_type:s},{byte_index:d}\n".format(display_name = item.fully_qualified_name, data_type = item.data_type, byte_index = item.buffer_start_byte))

# Load in the current device list and make sure that all devices have an assigned number
master_device_ids = {}

with open("telemetry/device_list.json") as fh:
    master_device_ids = json.load(fh)

telemetry_definition = {}

with open("telemetry/herb_garden_controller_tlm_definition.json") as fh:
    telemetry_definition = json.load(fh)

tlm_map = []

build_telemetry_points(tlm_map, telemetry_definition["telemetry_points"], "")

build_sub_device_points(tlm_map, telemetry_definition["sub_devices"], "", master_device_ids)

id_counter = 0
buffer_counter = 0
for item in tlm_map:
    item.assign_id(id_counter, buffer_counter)
    buffer_counter += Telemetry_item.DATA_TYPE_SIZES[item.data_type]
    id_counter += 1

generate_tlm_id_header(tlm_map, buffer_counter)
generate_tlm_map_file(tlm_map)
generate_tlm_decoder_map(tlm_map)